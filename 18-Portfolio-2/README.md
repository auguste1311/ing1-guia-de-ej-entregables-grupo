# Sobre el ejercicio y su bonus

Realizamos tanto el ejercicio como su bonus. El bonus tiene algunos cambios de estructura de reporte, que consideramos mejores que nuestra implementación del ejercicio obligatorio.

Archivos de ejercicio obligatorio: Portfolio-Solucion.st, CuisUniversity-4532.user.changes
Archivos de ejercicio bonus: Portfolio-Solucion-con-bonus-track.st, bonus.user.changes
