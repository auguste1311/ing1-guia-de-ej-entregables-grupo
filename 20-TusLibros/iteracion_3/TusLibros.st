!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:04'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:00'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:59'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'HernanWilkinson 6/17/2013 19:02'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #TusLibrosInterfaceTest category: 'TusLibros'!
TestCase subclass: #TusLibrosInterfaceTest
	instanceVariableNames: 'testObjectsFactory clockTime authenticationResult'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosInterfaceTest methodsFor: 'clock' stamp: 'AR 11/10/2021 18:40:14'!
time
	^ clockTime .! !


!TusLibrosInterfaceTest methodsFor: 'setup' stamp: 'AR 11/11/2021 10:18:51'!
setUp
	authenticationResult := true.
	testObjectsFactory := StoreTestObjectsFactory new.
	clockTime := testObjectsFactory today .! !


!TusLibrosInterfaceTest methodsFor: 'session tests' stamp: 'AR 11/11/2021 10:56:20'!
test12CannotListCartWhenSessionExpired

		| userInterface idCart |
		
		userInterface := self createDefaultInterface .
		
		idCart := userInterface createCartFor: 'an-valid-User' withPassword: 'an-valid-Pasword'.
		clockTime := testObjectsFactory today + 30 minutes + 1 seconds.
				
		self 
			should: [ userInterface listCart: idCart. ]
			raise: Error
			withMessageText: 'Session expired'.! !

!TusLibrosInterfaceTest methodsFor: 'session tests' stamp: 'AR 11/11/2021 10:56:34'!
test13ListCartRefreshesTheSessionDuration

		| userInterface idCart |
		
		"list cart refresh session"
		
		userInterface := self createDefaultInterface .
		
		clockTime := testObjectsFactory today.
		idCart := userInterface createCartFor: 'an-valid-User' withPassword: 'an-valid-Pasword'.
		clockTime := testObjectsFactory today + 29 minutes + 59 seconds.
		userInterface listCart: idCart.
		clockTime := testObjectsFactory today + 35 minutes.
		self assert: (userInterface listCart:		idCart) isEmpty.! !

!TusLibrosInterfaceTest methodsFor: 'session tests' stamp: 'AR 11/11/2021 10:56:50'!
test14CannotAddToCartWhenSessionExpired

		| userInterface idCart aBook|
		
		userInterface := self createDefaultInterface .
		
		idCart := userInterface createCartFor: 'an-valid-User' withPassword: 'an-valid-Pasword'.
		clockTime := testObjectsFactory today + 30 minutes + 1 seconds.
		
		aBook := testObjectsFactory itemSellByTheStore.
		
				
		self 
			should: [ 		userInterface addToCart: idCart thisBook: aBook withAQuantity: 1. ]
			raise: Error
			withMessageText: 'Session expired'.! !

!TusLibrosInterfaceTest methodsFor: 'session tests' stamp: 'AR 11/11/2021 10:57:07'!
test15AddToCartRefreshesTheSessionDuration

		| userInterface idCart aBook|
		
		"add cart refresh session"
		
		userInterface := self createDefaultInterface .
		
		clockTime := testObjectsFactory today.
		idCart := userInterface createCartFor: 'an-valid-User' withPassword: 'an-valid-Pasword'.
		clockTime := testObjectsFactory today + 29 minutes + 59 seconds.
		aBook := testObjectsFactory itemSellByTheStore.
		userInterface addToCart: idCart thisBook: aBook withAQuantity: 1.
		clockTime := testObjectsFactory today + 35 minutes.
		self assert: (userInterface listCart:		idCart) includes: aBook.! !

!TusLibrosInterfaceTest methodsFor: 'session tests' stamp: 'AR 11/11/2021 10:57:33'!
test16CannotCheckoutCartWhenSessionExpired

		| userInterface idCart aBook|
		
		"checkout cart refresh session"
		
		userInterface := self createDefaultInterface .
		idCart := userInterface createCartFor: 'an-valid-User' withPassword: 'an-valid-Pasword'.
		aBook := testObjectsFactory itemSellByTheStore.
		
		userInterface addToCart: idCart thisBook: aBook withAQuantity: 1.
		clockTime := testObjectsFactory today + 30 minutes + 1 seconds.
		
		self 
			should: [ 		userInterface addToCart: idCart thisBook: aBook withAQuantity: 1. ]
			raise: Error
			withMessageText: 'Session expired'.
		
		! !


!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'AR 11/11/2021 10:06:08'!
authenticate: aUser with: aPassword

	^ authenticationResult.! !

!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'ts 11/11/2021 11:24:51'!
createDefaultInterface

	^ TusLibrosInterface timedBy: self authenticatedBy: self withCatalog: (testObjectsFactory defaultCatalog) .! !

!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'AR 11/11/2021 10:03:43'!
invalidPasswordForUser
	^ 'an-invalid-password'.! !

!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'AR 11/11/2021 10:03:28'!
invalidUser
	^ 'an-invalid-user'.! !

!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'AR 11/11/2021 10:02:49'!
validPasswordForUser

	^ 'valid-password'! !

!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'AR 11/11/2021 10:02:33'!
validUser
	^ 'a-valid-user'.! !

!TusLibrosInterfaceTest methodsFor: 'utils' stamp: 'AR 11/11/2021 10:09:47'!
validate: aUser with: aPassword
	^ authenticationResult.! !


!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 11:54:43'!
test01CannotCreateCartForInvalidLogin		
		| userInterface |
		userInterface := self createDefaultInterface .
		authenticationResult := false.
		
		self
			should: [ 		
				userInterface createCartFor: self invalidUser withPassword: self invalidPasswordForUser .
				 ]
			raise: Error
			withMessageText: 'Invalid login information'! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'AR 11/11/2021 10:40:08'!
test02CanCreateCartForValidLogin

		| userInterface idCart |
		
		userInterface := self createDefaultInterface .
		
		idCart := userInterface createCartFor: self validUser withPassword: self validPasswordForUser .
		
		self assert: (userInterface listCart: idCart) isEmpty.! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'AR 11/11/2021 10:54:52'!
test02EmptyCartListsNoContents

		| userInterface |
						
		userInterface := self createDefaultInterface .
		userInterface createCartFor: self validUser withPassword: self validPasswordForUser .
		
		self assert: (userInterface listPurchasesFor: self validUser with:  self validPasswordForUser ) isEmpty.
! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'AR 11/11/2021 10:50:52'!
test03ListCartCanListCartContents

		| userInterface idCart aBook anotherBook cartContents |
		
		userInterface := self createDefaultInterface .
		
		idCart := userInterface createCartFor: self validUser withPassword: self validPasswordForUser .
		
		aBook := testObjectsFactory itemSellByTheStore.
		anotherBook := testObjectsFactory anotherItemSellByTheStore .
		
		userInterface addToCart: idCart thisBook: aBook withAQuantity: 1.
		userInterface addToCart: idCart thisBook: anotherBook withAQuantity: 2.
		
		cartContents := 		userInterface listCart: idCart.
		self assert: 1 equals: (cartContents occurrencesOf: aBook).
		self assert: 2 equals: (cartContents occurrencesOf: anotherBook).! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'AR 11/11/2021 10:51:30'!
test04DifferentCartsHaveTheirOwnContents

		| userInterface idCart1 idCart2 aBook |
		
		userInterface := self createDefaultInterface .
		
		idCart1 := userInterface createCartFor: 'an-valid-User' withPassword: 'an-valid-Pasword'.
		idCart2 := userInterface createCartFor: 'an-valid-User2' withPassword: 'an-valid-Pasword2'.
		
		aBook := testObjectsFactory itemSellByTheStore.
		
		userInterface addToCart: idCart1 thisBook: aBook withAQuantity: 1.
		
		self assert: (OrderedCollection with: aBook) 	equals: (userInterface listCart: idCart1).
		self assert: (userInterface listCart: idCart2) isEmpty .! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 11:53:38'!
test05WhenBookIsInvalidThenCannotAddItToCart

		| userInterface aBook |
		
		userInterface := self createDefaultInterface .
		
		aBook := testObjectsFactory itemSellByTheStore.
		
		self 
			should: [ userInterface addToCart: 42 thisBook: aBook withAQuantity: 1. ]
			raise: Error
			withMessageText: 'CartID not found'.! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 11:52:31'!
test06WhenCartIdIsInvalidCantListItsContents

		| userInterface |
		
		userInterface := self createDefaultInterface .
		
		self 
			should: [ userInterface listCart: 42. ]
			raise: Error
			withMessageText: 'CartID not found'.! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 11:52:47'!
test07WhenCartIdIsInvalidCannotCheckoutItsContents

		| userInterface |
		
		userInterface := self createDefaultInterface .
		
		self 
			should: [ userInterface checkOutCart: 42 with: testObjectsFactory for: self validUser ]
			raise: Error
			withMessageText: 'CartID not found'.! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'AR 11/11/2021 10:53:06'!
test08CheckoutPurchasesTheContentsOfThecart

		| userInterface idCart aBook |
		
		userInterface := self createDefaultInterface .
		
		idCart := userInterface createCartFor: self validUser withPassword: self validPasswordForUser .
		
		aBook := testObjectsFactory itemSellByTheStore.
		
		userInterface addToCart: idCart thisBook: aBook withAQuantity: 1.
		userInterface checkOutCart: idCart with: testObjectsFactory notExpiredCreditCard for: self validUser .
		
		self assert: (OrderedCollection with: aBook) equals: (userInterface listPurchasesFor: self validUser with: self validPasswordForUser ).
! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 11:55:15'!
test09WhenLoginIsInvalidThenCannotListPurchases

		| userInterface |		
		userInterface := self createDefaultInterface .
		authenticationResult := false.
		
		self 
			should: [ userInterface listPurchasesFor: self invalidUser with: self invalidPasswordForUser ]
			raise: Error
			withMessageText: 'Invalid login information'.
! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 12:10:58'!
test10WhenUserHasNoPurchasesThenNothingIsListed

		| userInterface |
				
		userInterface := self createDefaultInterface .
		
		self assert: (userInterface listPurchasesFor: self validUser  with:  self validPasswordForUser) isEmpty

! !

!TusLibrosInterfaceTest methodsFor: 'interface tests' stamp: 'ts 11/11/2021 12:16:22'!
test11DifferentCartsHaveTheirOwnPurchases

		| userInterface aBook idCart1 userId1 userId2 |
		
		userId1 := 'an-valid-User'.
		userId2 := 'another-valid-user'.
				
		userInterface := self createDefaultInterface .
		
		idCart1 := userInterface createCartFor: userId1 withPassword: 'a-valid-Pasword'.
		userInterface createCartFor: userId2 withPassword: 'another-valid-Pasword'.
		
		aBook := testObjectsFactory itemSellByTheStore.
		userInterface addToCart: idCart1 thisBook: aBook withAQuantity: 1.
		userInterface checkOutCart: idCart1 with: testObjectsFactory notExpiredCreditCard for: userId1. 
		
		self assert: (OrderedCollection with: aBook ) equals: (userInterface listPurchasesFor: userId1 with:  'a-valid-Pasword').
		self assert: (userInterface listPurchasesFor: userId2 with: 'another-valid-password') isEmpty .
! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:06'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'as yet unclassified' stamp: 'a 11/5/2021 16:42:46'!
listBooksOfCart

	^ items copy.	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:07'!
createSale

	^ Sale of: total
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
registerSale

	salesBook add: self createSale! !


!Cashier methodsFor: 'checkout' stamp: 'HernanWilkinson 6/17/2013 19:06'!
checkOut

	self calculateTotal.
	self debitTotal.
	self registerSale.

	^ total! !


!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:51'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 18:39'!
isExpiredOn: aDate 
	
	^expiration start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !


!CreditCard methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:38'!
initializeExpiringOn: aMonth 
	
	expiration := aMonth ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:47'!
initializeTotal: aTotal

	total := aTotal ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:47'!
of: aTotal

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aTotal ! !


!classDefinition: #SesionCart category: 'TusLibros'!
Object subclass: #SesionCart
	instanceVariableNames: 'lastModified cartProxee clock cartId'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!SesionCart methodsFor: 'initialization' stamp: 'ts 11/11/2021 12:19:40'!
initializeWithClock: aClock acceptingItemsOf: aCatalog withId: aCartId

	clock := aClock.
	cartProxee := Cart acceptingItemsOf: aCatalog.
	cartId := aCartId.
	lastModified := aClock time.
	! !


!SesionCart methodsFor: 'time analysis' stamp: 'ts 11/11/2021 12:39:43'!
assertSessionIsStillValid

	self thirtyMinutesHavePassed ifTrue: [self error: 'Session expired']! !

!SesionCart methodsFor: 'time analysis' stamp: 'ts 11/11/2021 11:18:19'!
refresh

	lastModified := clock time! !

!SesionCart methodsFor: 'time analysis' stamp: 'ts 11/11/2021 12:39:43'!
thirtyMinutesHavePassed

	^ clock time - lastModified > 30 minutes! !


!SesionCart methodsFor: 'proxy behaviour' stamp: 'ts 11/11/2021 11:43:29'!
doesNotUnderstand: aMessage
	
	|cartProxeeAnswer|
	self assertSessionIsStillValid.
	cartProxeeAnswer := cartProxee perform: aMessage selector withArguments: aMessage arguments.
	self refresh.
	^cartProxeeAnswer
	! !

!SesionCart methodsFor: 'proxy behaviour' stamp: 'ts 11/11/2021 12:19:10'!
validForId: aCartId

	^cartId = aCartId! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SesionCart class' category: 'TusLibros'!
SesionCart class
	instanceVariableNames: ''!

!SesionCart class methodsFor: 'as yet unclassified' stamp: 'ts 11/11/2021 12:19:58'!
withClock: aClock acceptingItemsOf: aCatalog withId: aCartId

	^self new initializeWithClock: aClock acceptingItemsOf: aCatalog withId: aCartId! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AR 11/11/2021 10:40:47'!
anotherItemSellByTheStore

	^ 'another-valid-book'.! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AR 11/11/2021 10:41:09'!
anotherItemSellByTheStorePrice
	
	^ 40.! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'AR 11/11/2021 10:50:24'!
defaultCatalog

	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice ;
		at: self anotherItemSellByTheStore put: self anotherItemSellByTheStorePrice ;
		yourself! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:37'!
expiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber - 1)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:36'!
notExpiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber + 1)! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !


!classDefinition: #TusLibrosInterface category: 'TusLibros'!
Object subclass: #TusLibrosInterface
	instanceVariableNames: 'aCartDictonary idCounter userToCartIdDictionary purchasesPerUser clock authenticationService merchantProcessor salesBook catalog sessionCarts'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosInterface methodsFor: 'interface' stamp: 'ts 11/11/2021 11:51:54'!
addToCart: aCartId thisBook: aBook withAQuantity: aQuantity 

	| aCart |
	aCart _ self accessCartWithId: aCartId.
	aCart add: aQuantity of: aBook.
	

	! !

!TusLibrosInterface methodsFor: 'interface' stamp: 'ts 11/11/2021 12:07:14'!
checkOutCart: aCartId with: aCreditCard for: aUserId

	| aCart |
	
	aCart _ self accessCartWithId: aCartId.
	
	Cashier toCheckout: aCart charging: aCreditCard throught: merchantProcessor on: clock time registeringOn: salesBook.
	
	(purchasesPerUser at: aUserId) addAll: aCart listBooksOfCart .! !

!TusLibrosInterface methodsFor: 'interface' stamp: 'ts 11/11/2021 12:38:32'!
createCartFor: aUser withPassword: aPassword 

	| aCart cartId|

	self authenticateUser: aUser withPassword: aPassword.
	
	cartId := self generateNextCartId.
	aCart _ SesionCart withClock: clock acceptingItemsOf: catalog withId: cartId.
	sessionCarts add: aCart.
	
	purchasesPerUser at: aUser ifAbsentPut: [OrderedCollection new].

	^ idCounter! !

!TusLibrosInterface methodsFor: 'interface' stamp: 'ts 11/11/2021 11:51:14'!
listCart: aCartId

	| aCart |
	
	aCart _ self accessCartWithId: aCartId.
	
	^ aCart listBooksOfCart.
	! !

!TusLibrosInterface methodsFor: 'interface' stamp: 'ts 11/11/2021 12:13:02'!
listPurchasesFor: aUserId with: aPassword 

	self authenticateUser: aUserId withPassword: aPassword.
		
	^purchasesPerUser at: aUserId ifAbsent: [^OrderedCollection new].! !


!TusLibrosInterface methodsFor: 'initialization' stamp: 'ts 11/11/2021 12:38:56'!
initializeWith: aClock authenticatedBy: anAuthenticationService withCatalog: aCatalog

	clock _ aClock.
	authenticationService _ anAuthenticationService .
	catalog _ aCatalog.
	sessionCarts _ OrderedCollection new.
	purchasesPerUser _ Dictionary new.
	idCounter := 0.
	
	
	
	! !


!TusLibrosInterface methodsFor: 'cart accessing' stamp: 'ts 11/11/2021 12:22:54'!
accessCartWithId: aCartId

	^sessionCarts detect: [:aCart | aCart validForId: aCartId] ifNone: [self error: 'CartID not found']! !

!TusLibrosInterface methodsFor: 'cart accessing' stamp: 'ts 11/11/2021 12:17:40'!
generateNextCartId

	^ idCounter := idCounter + 1! !


!TusLibrosInterface methodsFor: 'authentication' stamp: 'ts 11/11/2021 11:54:30'!
authenticateUser: aUser withPassword: aPassword

	^ (authenticationService validate: aUser with: aPassword) ifFalse:[ self error: 'Invalid login information']! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosInterface class' category: 'TusLibros'!
TusLibrosInterface class
	instanceVariableNames: ''!

!TusLibrosInterface class methodsFor: 'as yet unclassified' stamp: 'ts 11/11/2021 11:25:15'!
timedBy: aClock authenticatedBy: anAuthenticationService withCatalog: aCatalog

	^ self new initializeWith: aClock authenticatedBy: anAuthenticationService withCatalog: aCatalog.! !
