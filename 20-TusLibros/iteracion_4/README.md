Los pasos para correr nuestro carrito son:

- Hacer fileIn de los archivos .st que se encuentran en este directorio.
- Correr en un workspace los siguientes comandos:

```
    TusLibrosServer allInstances do:[:instance | instance stopListening . instance destroy.]

    server := TusLibrosServer listeningOn: 8080.
    TusLibrosLoginWindow open.
```

- Loggearse con user: validUser y password: validUserPassword