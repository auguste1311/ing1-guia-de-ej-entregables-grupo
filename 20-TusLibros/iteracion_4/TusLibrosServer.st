!classDefinition: #TusLibrosCartContentsWindow category: 'TusLibrosServer'!
Panel subclass: #TusLibrosCartContentsWindow
	instanceVariableNames: 'cartItems'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 00:39:25'!
addToCartButton

	| addItemButton itemAmountField row |
	addItemButton := PluggableButtonMorph model: self model stateGetter: nil action: #addItemToCart label: 'Add selected item to cart'.
	
	itemAmountField := TextModelMorph textProvider: self model textGetter: #itemAmount textSetter: #itemAmount:. 
	itemAmountField innerTextMorph setProperty: #keyStroke: toValue: [ :key | itemAmountField innerTextMorph acceptContents ] .
	itemAmountField  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 1. 
	itemAmountField layoutSpec fixedHeight: 30.
	
	
	row := LayoutMorph newRow.
	
	row addMorph: itemAmountField ;
	addMorph: addItemButton .
	^ row.! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 00:42:37'!
buildCartContentsColumn

	|  columnMorph |
	cartItems := PluggableListMorph model: self model listGetter: #cartContent indexGetter: #cartContentIndex indexSetter: #cartContentIndex:.
	cartItems  borderColor: Color skyBlue; borderWidth: 1; morphWidth: 300.
	
	cartItems layoutSpec proportionalHeight: 1.
	
	columnMorph := LayoutMorph newColumn .
	columnMorph separation: 25;
	axisEdgeWeight: 0.5;
	addMorph: cartItems;
	addMorph: (LabelMorph contents: 'Items currently in cart');
	addMorph: self removeFromCartButton .
	
	^columnMorph.! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/27/2021 13:29:27'!
buildCatalogColumn

	|  columnMorph catalogList |
	catalogList := PluggableListMorph model: self model listGetter: #catalog indexGetter: #catalogIndex indexSetter: #catalogIndex:.
	catalogList  borderColor: Color skyBlue; borderWidth: 1; morphWidth: 300.
	
	catalogList layoutSpec proportionalHeight: 0.85.
	
	columnMorph := LayoutMorph newColumn .
	columnMorph separation: 25;
	axisEdgeWeight: 0.5;
	addMorph: catalogList;
	addMorph: (LabelMorph contents: 'Items available in catalog');
	addMorph: self addToCartButton.
	
	^columnMorph.! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 00:40:13'!
buildCheckOutButton

	| checkoutItemButton |
	checkoutItemButton := PluggableButtonMorph model: self model stateGetter: nil action: #checkoutCart label: 'Checkout Cart'.
	checkoutItemButton layoutSpec proportionalWidth: 0.8.
	
	^ checkoutItemButton.! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 00:40:36'!
buildMorphicWindow
		
	self layoutMorph beRow;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: self buildCatalogColumn;
	addMorph: self buildCartContentsColumn;
	addMorph: self buildCheckOutButton.! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/27/2021 13:13:00'!
defaultExtent

	^ 1035@485! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 00:41:09'!
removeFromCartButton

	| removeItemButton |
	removeItemButton := PluggableButtonMorph model: self model stateGetter: nil action: #removeItemFromCart label: 'Remove'.
	removeItemButton layoutSpec proportionalWidth: 0.8.
	
	^ removeItemButton.! !

!TusLibrosCartContentsWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 14:51:56'!
updateCartContents

	self model updateCartList.

	cartItems updateList.! !


!TusLibrosCartContentsWindow methodsFor: 'initialization' stamp: 'AR 11/29/2021 00:30:56'!
initializeWith: aTitle for: cartId modeledBy: aModel

	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: aTitle.
	self model: aModel.
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.

	self model when: #showCartItem send: #updateCartContents to: self.

! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosCartContentsWindow class' category: 'TusLibrosServer'!
TusLibrosCartContentsWindow class
	instanceVariableNames: ''!

!TusLibrosCartContentsWindow class methodsFor: 'as yet unclassified' stamp: 'AR 11/27/2021 11:47:53'!
openWindowFor: cartId modeledBy: model
	
	^self new initializeWith: 'Tus Libros Carrito' for: cartId modeledBy: model.! !


!classDefinition: #TusLibrosCheckoutWindow category: 'TusLibrosServer'!
Panel subclass: #TusLibrosCheckoutWindow
	instanceVariableNames: 'l'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosCheckoutWindow methodsFor: 'initialization' stamp: 'AR 11/29/2021 01:08:26'!
initializeWith: checkoutModel

	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: 'Checkout'.
	self model: checkoutModel .
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.! !


!TusLibrosCheckoutWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 01:09:08'!
buildMorphicWindow

	self layoutMorph beRow;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: self checkoutScreen.! !

!TusLibrosCheckoutWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 15:51:58'!
checkoutScreen

	| columnMorph |
	columnMorph := LayoutMorph newColumn .
	columnMorph separation: 25;
	axisEdgeWeight: 0.5;
	
	borderWidth: 1; morphWidth: 300.
	
	columnMorph layoutSpec proportionalHeight: 0.85.
	
	(self model writeTicket) do: [:entry | | entryMorph | 
		entryMorph := (LabelMorph contents: entry ).
		columnMorph addMorph: entryMorph.].

	^ columnMorph! !

!TusLibrosCheckoutWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 01:17:55'!
defaultExtent

	^ 1035@485! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosCheckoutWindow class' category: 'TusLibrosServer'!
TusLibrosCheckoutWindow class
	instanceVariableNames: ''!

!TusLibrosCheckoutWindow class methodsFor: 'as yet unclassified' stamp: 'AR 11/29/2021 01:06:35'!
openWindowModeledBy: checkoutModel

	^ self new initializeWith: checkoutModel.! !


!classDefinition: #TusLibrosLoginWindow category: 'TusLibrosServer'!
Panel subclass: #TusLibrosLoginWindow
	instanceVariableNames: 'wordsListMorph failedLoginAtLeastOnce'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosLoginWindow methodsFor: 'initialization' stamp: 'AR 11/15/2021 21:10:26'!
defaultExtent

	^ 1035@485
	! !

!TusLibrosLoginWindow methodsFor: 'initialization' stamp: 'AR 11/29/2021 15:39:27'!
initializeWith: aTitle

	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: aTitle.
	self model: (TusLibrosLoginModel new).
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.
	
	self model when: #InvalidLoginCredentials send: #buildLoginError to: self.
	self model when: #SuccessfulLogin send: #closeWindow to: self.
	failedLoginAtLeastOnce := false.
			
	"self model when: #newWordsArrived send: #refreshListOfWords to: self."
	
	"Investigar:
	self model when: #newWordsArrived send: #refreshListOfWords:and: to: self."! !


!TusLibrosLoginWindow methodsFor: 'access' stamp: 'AR 11/15/2021 21:10:26'!
refreshListOfWords
	
	wordsListMorph updateList.
	wordsListMorph setSelectionIndex: 0.
	! !


!TusLibrosLoginWindow methodsFor: 'GUI building' stamp: 'AR 11/15/2021 21:35:07'!
build1stRow
	|  firstRowLayoutMorph sentenceTextBoxMorph |
		
	sentenceTextBoxMorph := TextModelMorph textProvider: self model textGetter: #userText textSetter: #userText:. 
	sentenceTextBoxMorph innerTextMorph setProperty: #keyStroke: toValue: [ :key | sentenceTextBoxMorph innerTextMorph acceptContents ] .
	sentenceTextBoxMorph  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 300. 
		
	firstRowLayoutMorph := LayoutMorph newRow.
	firstRowLayoutMorph separation: 25;
	axisEdgeWeight: 0.5;
	addMorph: (LabelMorph contents:'Enter your User Name:');
	addMorph: sentenceTextBoxMorph.
	
	^firstRowLayoutMorph.! !

!TusLibrosLoginWindow methodsFor: 'GUI building' stamp: 'ts 11/27/2021 16:36:00'!
build2ndRow
	|  firstRowLayoutMorph sentenceTextBoxMorph|
		
	sentenceTextBoxMorph := TextModelMorph textProvider: self model textGetter: #passwordText textSetter: #passwordText:. 
	sentenceTextBoxMorph innerTextMorph setProperty: #keyStroke: toValue: [ :key | sentenceTextBoxMorph innerTextMorph acceptContents ] .
	sentenceTextBoxMorph  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 300. 
		
	firstRowLayoutMorph := LayoutMorph newRow.
	firstRowLayoutMorph separation: 20;
	axisEdgeWeight: 0.5;
	addMorph: (LabelMorph contents:'Enter your password:');
	addMorph: sentenceTextBoxMorph.
	
	^firstRowLayoutMorph.! !

!TusLibrosLoginWindow methodsFor: 'GUI building' stamp: 'ts 11/28/2021 21:27:55'!
build3rdRow

	| loginButton thirdRowLayoutMorph|

	loginButton := PluggableButtonMorph model: self model stateGetter: nil  action: #createCart label: 'Create Cart'.	

	thirdRowLayoutMorph := LayoutMorph newRow.
	thirdRowLayoutMorph separation: 15;
	axisEdgeWeight: 0.5;
	addMorph: loginButton.
	
	^thirdRowLayoutMorph.! !

!TusLibrosLoginWindow methodsFor: 'GUI building' stamp: 'ts 11/28/2021 21:26:51'!
buildLoginError
	
	failedLoginAtLeastOnce ifFalse: [self layoutMorph addMorphFront:  (LabelMorph contents:'Invalid Login Credentials')].
	
	failedLoginAtLeastOnce := true.! !

!TusLibrosLoginWindow methodsFor: 'GUI building' stamp: 'AR 11/15/2021 21:18:56'!
buildMorphicWindow
		
	self layoutMorph beColumn;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: self build1stRow;
	addMorph: self build2ndRow;
	addMorph: self build3rdRow.
	! !

!TusLibrosLoginWindow methodsFor: 'GUI building' stamp: 'AR 11/29/2021 15:38:46'!
closeWindow

    self closeButtonClicked.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosLoginWindow class' category: 'TusLibrosServer'!
TusLibrosLoginWindow class
	instanceVariableNames: ''!

!TusLibrosLoginWindow class methodsFor: 'instance creation' stamp: 'AR 11/15/2021 21:12:56'!
open
	
	^self new initializeWith: 'Tus Libros'.! !


!classDefinition: #TusLibrosCartContentsModel category: 'TusLibrosServer'!
Object subclass: #TusLibrosCartContentsModel
	instanceVariableNames: 'restInterface cartId catalog catalogIndex catalogItems cartContent cartContentIndex itemAmount isbnWithTitle isbnWithTitleToPrice isbnToPrice cartDictionary priceDictionary'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosCartContentsModel methodsFor: 'utils' stamp: 'AR 11/29/2021 00:49:26'!
buildIsbnDictionaries

	isbnWithTitle := OrderedCollection new.
	priceDictionary := Dictionary new.
	catalog keysAndValuesDo: [:aKey :aValue | 
		isbnWithTitle add: aKey,'->', (aValue copyUpTo: $-).
		priceDictionary at: aKey put: (aValue copyAfter: $-) asNumber.
		].
! !


!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/29/2021 00:24:50'!
cartContent

	^ cartContent.! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 13:08:06'!
cartContentIndex

	^ cartContentIndex.! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 13:08:37'!
cartContentIndex: anIndex

  	cartContentIndex _ anIndex.

  	self changed: #cartContentIndex. ! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 15:10:50'!
catalog
	
	^ isbnWithTitle .! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 11:53:40'!
catalogIndex

	^ catalogIndex .! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 13:01:03'!
catalogIndex: anIndex

  	catalogIndex _ anIndex.

  	self changed: #catalogIndex. ! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 13:36:23'!
itemAmount

	^ itemAmount.! !

!TusLibrosCartContentsModel methodsFor: 'accessors' stamp: 'AR 11/27/2021 13:36:59'!
itemAmount: anAmount

	itemAmount := anAmount .
	^true.! !


!TusLibrosCartContentsModel methodsFor: 'checkout' stamp: 'AR 11/29/2021 15:10:18'!
checkoutCart

	|total checkoutModel |
	total := 0.

	total := restInterface sendCheckoutFor: cartId .
	
	checkoutModel := TusLibrosCheckoutModel buying: cartDictionary priced: total.
	
	TusLibrosCheckoutWindow openWindowModeledBy: checkoutModel.

	! !


!TusLibrosCartContentsModel methodsFor: 'cart operations' stamp: 'AR 11/29/2021 00:21:40'!
addCatalogItem: anItem times: aQuantity

	catalog at: anItem put: aQuantity .
	^true.! !

!TusLibrosCartContentsModel methodsFor: 'cart operations' stamp: 'AR 11/29/2021 14:49:05'!
addItemToCart

	| isbn amount |
	
	isbn := (isbnWithTitle at: catalogIndex) copyUpTo: $- .
	amount := itemAmount asNumber.
	
	"cartDictionary at: isbn put: amount.
	(cartContent includes: isbn) ifFalse: [cartContent add: isbn]."
	restInterface sendAddToCartRequestFor: cartId buying: isbn times: amount.
	self triggerEvent: #showCartItem with: self.
	
	"restInterface sendAddToCartRequestFor: cartId buying: isbn times: amount asString.
	"! !

!TusLibrosCartContentsModel methodsFor: 'cart operations' stamp: 'AR 11/27/2021 01:51:08'!
removeCatalogItem: anItem

	catalog removeKey: anItem.
	^true.! !

!TusLibrosCartContentsModel methodsFor: 'cart operations' stamp: 'AR 11/29/2021 15:44:34'!
removeItemFromCart
	|isbn|

	isbn := (cartContent at: cartContentIndex) copyUpTo: $- .
	
	restInterface sendRemoveFor: cartId item: isbn times: 1.
	
	self triggerEvent: #showCartItem with: self.! !

!TusLibrosCartContentsModel methodsFor: 'cart operations' stamp: 'AR 11/29/2021 14:52:24'!
updateCartList

	cartContent := restInterface sendListCartFor: cartId.! !


!TusLibrosCartContentsModel methodsFor: 'initialization' stamp: 'AR 11/29/2021 00:44:45'!
initializeFor: aCartId

	cartId := aCartId .
	restInterface := TusLibrosRestInterface new.
	catalog := restInterface sendReadCatalogOf: aCartId.
	self buildIsbnDictionaries.
	catalogIndex := 1.
	catalogItems := catalog keys.
	cartContent := OrderedCollection new.
	cartContentIndex := 1.
	itemAmount := '0'.
	
	cartDictionary := Dictionary new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosCartContentsModel class' category: 'TusLibrosServer'!
TusLibrosCartContentsModel class
	instanceVariableNames: ''!

!TusLibrosCartContentsModel class methodsFor: 'as yet unclassified' stamp: 'AR 11/27/2021 01:24:16'!
for: aCartId

	^ self new initializeFor: aCartId.! !


!classDefinition: #TusLibrosCheckoutModel category: 'TusLibrosServer'!
Object subclass: #TusLibrosCheckoutModel
	instanceVariableNames: 'cartDictionary total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosCheckoutModel methodsFor: 'checkout' stamp: 'AR 11/29/2021 15:51:39'!
writeTicket

	| ticket |
	ticket := OrderedCollection new.
	
	cartDictionary keysAndValuesDo: [:isbn :quantity | 
		ticket add: 'Bougth book with ISBN: ' , isbn, ' and quantity: ', quantity asString].
	
	ticket add: 'Total price is: ' , total asString.
	^ ticket.! !


!TusLibrosCheckoutModel methodsFor: 'initialization' stamp: 'AR 11/29/2021 01:05:07'!
initializeWith: aCartDictionary priced: aTotal

	cartDictionary := aCartDictionary .
	total := aTotal .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosCheckoutModel class' category: 'TusLibrosServer'!
TusLibrosCheckoutModel class
	instanceVariableNames: ''!

!TusLibrosCheckoutModel class methodsFor: 'as yet unclassified' stamp: 'AR 11/29/2021 01:04:44'!
buying: cartDictionary priced: total

	^ self new initializeWith: cartDictionary priced: total.! !


!classDefinition: #TusLibrosLoginModel category: 'TusLibrosServer'!
Object subclass: #TusLibrosLoginModel
	instanceVariableNames: 'sentence words selectedIndex restInterface password user'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosLoginModel methodsFor: 'initialization' stamp: 'AR 11/15/2021 21:36:23'!
initialize

	user := ''.
	password := ''.
	words := OrderedCollection new.
	sentence := ''.
	selectedIndex := 0.
	restInterface := TusLibrosRestInterface new.! !


!TusLibrosLoginModel methodsFor: 'TextModelMorphSelectors' stamp: 'AR 11/15/2021 21:25:49'!
passwordText
	
	^password.! !

!TusLibrosLoginModel methodsFor: 'TextModelMorphSelectors' stamp: 'AR 11/15/2021 21:26:35'!
passwordText: aPassword

	password := aPassword.
	^true.! !

!TusLibrosLoginModel methodsFor: 'TextModelMorphSelectors' stamp: 'AR 11/15/2021 21:25:58'!
userText
	
	^user.! !

!TusLibrosLoginModel methodsFor: 'TextModelMorphSelectors' stamp: 'AR 11/15/2021 21:26:04'!
userText: aUser

	user := aUser.
	^true.! !


!TusLibrosLoginModel methodsFor: 'cart operations' stamp: 'AR 11/29/2021 15:40:23'!
createCart

	| cartId cartContentModel |
	
	cartId := [self getCartIdFromRestInterface] on: Error do: [^self triggerEvent: #InvalidLoginCredentials].
	
	cartContentModel := TusLibrosCartContentsModel for: cartId.
	
	TusLibrosCartContentsWindow openWindowFor: cartId asString modeledBy: cartContentModel.
	self triggerEvent: #SuccessfulLogin.
	
! !

!TusLibrosLoginModel methodsFor: 'cart operations' stamp: 'ts 11/28/2021 21:25:17'!
getCartIdFromRestInterface

	
	| restInterfaceResponse cartIdAsString|
	restInterfaceResponse := restInterface 	sendCreateCartRequestFor: user with: password.
	
	cartIdAsString := ''.
	restInterfaceResponse do: [:aIntChar | cartIdAsString := cartIdAsString , aIntChar asString].
	
	^cartIdAsString
	
	
! !


!classDefinition: #TusLibrosRestInterface category: 'TusLibrosServer'!
Object subclass: #TusLibrosRestInterface
	instanceVariableNames: 'tusLibros port webServer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosRestInterface methodsFor: 'utils' stamp: 'AR 11/15/2021 20:30:21'!
correctlyEncodeSpacesForUrlRequestParameter: aParameter
	
	^ aParameter copyReplaceAll: ' ' with: '%20'. ! !


!TusLibrosRestInterface methodsFor: 'accessing' stamp: 'AR 11/29/2021 15:55:18'!
port

	^port ifNil: [port:=8080].! !

!TusLibrosRestInterface methodsFor: 'accessing' stamp: 'AR 11/15/2021 20:30:03'!
url
	
	^'http://localhost:', self port asString! !


!TusLibrosRestInterface methodsFor: 'server communication' stamp: 'AR 11/29/2021 00:59:54'!
sendAddToCartRequestFor: aCartId buying: aBookIsbn times: aBookQuantity 

	| fieldDict resp urlEncodedBookIsbn|

	urlEncodedBookIsbn := self correctlyEncodeSpacesForUrlRequestParameter: aBookIsbn.
	fieldDict := Dictionary newFromPairs: {'cartId'. aCartId . 'bookIsbn'. urlEncodedBookIsbn. 'bookQuantity'. aBookQuantity }.		
	
	resp:= WebClient htmlSubmit: (self url,'/addToCart') fields: fieldDict.
		
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: ((resp content) readStream)) asOrderedCollection .] 
		ifFalse:[^self error: resp content].! !

!TusLibrosRestInterface methodsFor: 'server communication' stamp: 'AR 11/29/2021 15:20:17'!
sendCheckoutFor: aCartId

	| fieldDict resp urlEncodedUser |

	urlEncodedUser := self correctlyEncodeSpacesForUrlRequestParameter: aCartId .
	fieldDict := Dictionary newFromPairs: {'cartId'. urlEncodedUser. 'cced'. '2021-11'. 'ccn'. '123'. 'cco'. 'validUser'. }.		
	
	resp:= WebClient htmlSubmit: (self url,'/checkOutCart') fields: fieldDict.
	
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: ((resp content) readStream)) asNumber .] 
		ifFalse:[^self error: resp content].! !

!TusLibrosRestInterface methodsFor: 'server communication' stamp: 'ts 11/27/2021 17:00:24'!
sendCreateCartRequestFor: aClientId with: aPassword 

	| fieldDict resp urlEncodedUser urlEncodedPassword |

	urlEncodedUser := self correctlyEncodeSpacesForUrlRequestParameter: aClientId .
	urlEncodedPassword := self correctlyEncodeSpacesForUrlRequestParameter: aPassword.
	fieldDict := Dictionary newFromPairs: {'clientId'. urlEncodedUser . 'password'. urlEncodedPassword }.		
	
	resp:= WebClient htmlSubmit: (self url,'/createCart') fields: fieldDict.
	
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: ((resp content) readStream)) asOrderedCollection.] 
		ifFalse:[^self error: resp content].! !

!TusLibrosRestInterface methodsFor: 'server communication' stamp: 'AR 11/29/2021 15:04:28'!
sendListCartFor: aCartId

	| fieldDict resp |

	fieldDict := Dictionary newFromPairs: {'cartId'. aCartId .}.		
	
	resp:= WebClient htmlSubmit: (self url,'/listCart') fields: fieldDict.
		
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: ((resp content) readStream)) asOrderedCollection .] 
		ifFalse:[^self error: resp content].! !

!TusLibrosRestInterface methodsFor: 'server communication' stamp: 'AR 11/27/2021 01:47:38'!
sendReadCatalogOf: aCartId

	| fieldDict resp urlEncodedUser |

	urlEncodedUser := self correctlyEncodeSpacesForUrlRequestParameter: aCartId .
	fieldDict := Dictionary newFromPairs: {'cartId'. urlEncodedUser }.		
	
	resp:= WebClient htmlSubmit: (self url,'/readCatalog') fields: fieldDict.
	
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: ((resp content) readStream)) asDictionary .] 
		ifFalse:[^self error: resp content].! !

!TusLibrosRestInterface methodsFor: 'server communication' stamp: 'AR 11/29/2021 15:26:37'!
sendRemoveFor: aCartId item: anIsbn times: aQuantity

	| fieldDict resp urlEncodedBookIsbn|

	urlEncodedBookIsbn := self correctlyEncodeSpacesForUrlRequestParameter: anIsbn.
	fieldDict := Dictionary newFromPairs: {'cartId'. aCartId . 'bookIsbn'. urlEncodedBookIsbn. 'bookQuantity'. aQuantity }.		
	
	resp:= WebClient htmlSubmit: (self url,'/removeFromCart') fields: fieldDict.
		
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: ((resp content) readStream)) asOrderedCollection .] 
		ifFalse:[^self error: resp content].! !


!classDefinition: #TusLibrosServer category: 'TusLibrosServer'!
Object subclass: #TusLibrosServer
	instanceVariableNames: 'webServer port'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosServer'!

!TusLibrosServer methodsFor: 'listening' stamp: 'AR 11/15/2021 20:46:42'!
startListening
	
	webServer startListener.

	^'Listening on port: ', self port asString.
	! !

!TusLibrosServer methodsFor: 'listening' stamp: 'AR 11/15/2021 20:46:50'!
stopListening
	
	webServer stopListener.
	
	^'Stopped listening from port: ', self port asString! !


!TusLibrosServer methodsFor: 'initialization' stamp: 'AR 11/29/2021 15:36:40'!
initializeWith: aPortNumber
	
	|testObject tusLibros |
	
	testObject := TusLibrosSystemFacadeTest new.
	testObject setUp.
	tusLibros := testObject systemFacade .
	
	port:= aPortNumber.
	
	webServer := WebServer new listenOn: self port.
	
	webServer addService: '/createCart' action: [:request | |user password cartId cartIdJson|	
		[user := request fields at: 'clientId'.
		password := request fields at: 'password'.
		cartId := tusLibros createCartFor: user authenticatedWith: password.
		cartIdJson := WebUtils jsonEncode: (cartId asString).
		request send200Response: cartIdJson ]
	on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	webServer addService: '/readCatalog' action: [:request | |cart catalog |	
		[ | catalogJson catalogStream jsonFriendlyCatalog |
		cart := (request fields at: 'cartId') asNumber.
		catalog := tusLibros readCartCatalogOf: cart.
		jsonFriendlyCatalog := Dictionary new.
		catalog keysAndValuesDo: [:aKey :aValue | jsonFriendlyCatalog at: aKey asString put: (aValue title,'-',aValue price asString)].
		catalogStream := ReadWriteStream on: ''.
		WebUtils jsonMap: jsonFriendlyCatalog on: catalogStream .
		catalogJson := catalogStream contents.
		request send200Response: catalogJson ]
	on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	webServer addService: '/addToCart' action: [:request | |cartId bookIsbn bookQuantity result|	
		[cartId := (request fields at: 'cartId') asNumber .
		bookIsbn := request fields at: 'bookIsbn'.
		bookQuantity := (request fields at: 'bookQuantity') asNumber.
		tusLibros add: bookQuantity of: bookIsbn toCartIdentifiedAs: cartId .
		result := WebUtils jsonEncode: 'OK'.
		request send200Response: result ]
	on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	webServer addService: '/removeFromCart' action: [:request | |cartId bookIsbn bookQuantity result|	
		[
		cartId := (request fields at: 'cartId') asNumber .
		bookIsbn := request fields at: 'bookIsbn'.
		bookQuantity := (request fields at: 'bookQuantity') asNumber.
		tusLibros remove: bookQuantity of: bookIsbn toCartIdentifiedAs: cartId .
		result := WebUtils jsonEncode: 'OK'.
		request send200Response: result ]
	on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	webServer addService: '/listCart' action: [:request | |cartId cartContents cartContentsJson cartStream|	
		[cartId := (request fields at: 'cartId') asNumber .
		cartContents := (tusLibros listCartIdentifiedAs: cartId).
		cartStream := ReadWriteStream on: ''.
		WebUtils jsonArray: cartContents on: cartStream .
		cartContentsJson := cartStream contents.
		request send200Response: (cartContentsJson) ]
	on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	webServer addService: '/checkOutCart' action:[:request | | cartId creditCardNumber creditCardExpirationDate creditCardOwner result|
		[ | creditCardExpirationMonth stream total |
		cartId := (request fields at: 'cartId') asNumber .
		creditCardNumber := (request fields at: 'ccn') asNumber .
		creditCardExpirationDate :=  (request fields at: 'cced').
		stream := ReadStream on: creditCardExpirationDate .
		creditCardExpirationMonth := Month readFrom: stream .
		creditCardOwner := (request fields at: 'cco').
		total := tusLibros checkOutCartIdentifiedAs: cartId 
		withCreditCardNumbered: creditCardNumber 
		ownedBy: creditCardOwner 
		expiringOn: creditCardExpirationMonth.
		result := WebUtils jsonEncode: total asString.
		request send200Response: result.
			]
			on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	webServer addService: '/listPurchases' action: [:request | |clientId password|	
		[ | purchases purchaseStream purchasesJson |clientId := (request fields at: 'clientId') .
		password := (request fields at: 'password').
		purchases := tusLibros listPurchasesOf: clientId authenticatingWith: password.
		purchaseStream := ReadWriteStream on: ''.
		WebUtils jsonMap: purchases on: purchaseStream .
		purchasesJson := purchaseStream contents.
		request send200Response: (purchasesJson ) ]
	on: Error
		do: [: anError | request send400Response: anError messageText]
		].
	
	! !


!TusLibrosServer methodsFor: 'destruction' stamp: 'AR 11/15/2021 20:50:31'!
destroy
	
	webServer ifNotNil:[webServer destroy].! !


!TusLibrosServer methodsFor: 'accessing' stamp: 'AR 11/15/2021 20:47:01'!
port

	^port ifNil: [port:=8080].
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosServer class' category: 'TusLibrosServer'!
TusLibrosServer class
	instanceVariableNames: ''!

!TusLibrosServer class methodsFor: 'as yet unclassified' stamp: 'AR 11/15/2021 20:49:27'!
listeningOn: aPortNumber

	^self new initializeWith: aPortNumber.! !
