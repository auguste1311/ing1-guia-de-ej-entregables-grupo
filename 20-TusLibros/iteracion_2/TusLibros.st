!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:17:36'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:17:47'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:19:25'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:17:58'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:18:03'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:18:10'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:18:15'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'AR 11/3/2021 22:18:23'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !

!CartTest methodsFor: 'tests' stamp: 'ts 11/4/2021 15:12:38'!
test09EmptyCartHasPriceCero

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self assert: 0 equals: cart price. ! !

!CartTest methodsFor: 'tests' stamp: 'ts 11/4/2021 15:13:07'!
test10CartWithOneItemHasAPriceEqualToThatItemPrice

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 1 of: testObjectsFactory itemSellByTheStore.
	self assert: testObjectsFactory itemSellByTheStorePrice equals: cart price. ! !

!CartTest methodsFor: 'tests' stamp: 'ts 11/4/2021 15:13:36'!
test11CartWithMultipleItemsHasPriceEqualToTheSumOfItsItemsPrice

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: testObjectsFactory itemSellByTheStorePrice * 2 equals: cart price. ! !


!CartTest methodsFor: 'setup' stamp: 'AR 11/3/2021 22:16:47'!
setUp

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory cartWithTwoItems cartWithOneItem validCreditCard aFixedDateOnThe3rdOfNovember2021 stolenCreditCard creditCardWithNoFunds expiredCreditCard'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:25:59'!
setUp

	testObjectsFactory := StoreTestObjectsFactory new.
	cartWithTwoItems := testObjectsFactory createCart add: 2 of: testObjectsFactory itemSellByTheStore.
	cartWithOneItem := testObjectsFactory createCart add: 1 of: testObjectsFactory itemSellByTheStore.
	validCreditCard := testObjectsFactory validCreditCard .
	expiredCreditCard := testObjectsFactory expiredCreditCard .
	aFixedDateOnThe3rdOfNovember2021 := (FixedGregorianDate yearNumber: 2021 monthNumber: 11 dayNumber: 3).
	stolenCreditCard := testObjectsFactory stolenCreditCard .
	creditCardWithNoFunds := testObjectsFactory creditCardWithoutFunds .! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:22:42'!
test01WhenCheckingOutSomethingThatIsNotACartShouldPromptInvalidCartError

	| cashier salesLog|
	
	cashier := Cashier new.
	salesLog := OrderedCollection new.
		
	self 
		should: [ cashier checkout: 'not-a-cart' with: validCreditCard on: (10/10/2021) registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot checkout something that is not a Cart' equals: anError messageText.
			self assert: salesLog isEmpty]! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:23:28'!
test02WhenCheckingOutWithSomethingThatIsNotACreditCardShouldPromptInvalidCreditCardError

	| cashier salesLog |
	
	cashier := Cashier new.
	salesLog := OrderedCollection new.
		
	self 
		should: [ cashier checkout: cartWithOneItem with: 'not-a-credit-card' on: aFixedDateOnThe3rdOfNovember2021 registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot checkout with something that isnt a Credit Card' equals: anError messageText.
			self assert: salesLog isEmpty ]! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:24:00'!
test03WhenCheckingOutOnSomethingThatIsNotADateShouldPromptInvalidDateError

	| cashier salesLog |
	
	cashier := Cashier new.
	salesLog := OrderedCollection new.
		
	self 
		should: [ cashier checkout: cartWithOneItem with: validCreditCard on: 'not-a-date' registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot checkout on an invalid date' equals: anError messageText.
			self assert: salesLog isEmpty.]! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:24:56'!
test04WhenCkeckingOutAnEmptyCartShouldPromptEmptyCartError

	| emptyCart cashier salesLog|
	
	emptyCart := testObjectsFactory createCart.
	cashier := Cashier new.
	salesLog := OrderedCollection new.
		
	self 
		should: [ cashier checkout: emptyCart with: validCreditCard on: aFixedDateOnThe3rdOfNovember2021 registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot checkout an empty cart' equals: anError messageText.
			self assert: salesLog isEmpty.]! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:33:24'!
test05WhenCheckingOutWithAnExpiredCreditCardShouldPromptExperidCreditCardError

	| cashier salesLog|
	
	salesLog := OrderedCollection new.
	cashier := Cashier new.
		
	self 
		should: [ cashier checkout: cartWithOneItem with: expiredCreditCard on: testObjectsFactory dateAfterExpiration registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot checkout with an expired Credit Card' equals: anError messageText.
			self assert: salesLog isEmpty.]! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:27:41'!
test06CkeckingOutASingleItemShouldReturnAcheckoutPriceEqualToTheItemPrice

	| cashier checkoutPrice salesLog |
	
	salesLog := OrderedCollection new.
	cashier := Cashier new.
	
	checkoutPrice := cashier checkout: cartWithOneItem 
	with: validCreditCard 
	on: aFixedDateOnThe3rdOfNovember2021 
	registeringOn: salesLog.
	
	self assert: testObjectsFactory itemSellByTheStorePrice equals: checkoutPrice.
	self assert: cartWithOneItem price equals: checkoutPrice.
	self assert: 1 equals: salesLog size.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:28:16'!
test07CheckingOutMultipleItemsShouldReturnACheckoutPriceEqualToTheSumOfTheItemPrices

	| cashier checkoutPrice salesLog |
	
	salesLog := OrderedCollection new.
	cashier := Cashier new.
	
	checkoutPrice := cashier checkout: cartWithTwoItems 
	with: validCreditCard 
	on: aFixedDateOnThe3rdOfNovember2021 
	registeringOn: salesLog.
	
	self assert: testObjectsFactory itemSellByTheStorePrice * 2 equals: checkoutPrice.
	self assert: cartWithTwoItems price equals: checkoutPrice.
	self assert: 1 equals: salesLog size.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:31:08'!
test08CheckingOutMultipletimesShouldIncreaseTheSalesLogSizeProperly

	| cashier checkoutPriceForCartWithTwoItems checkoutPriceForCartWithOneItem salesLog |
	
	salesLog := OrderedCollection new.
	cashier := Cashier new.
	
	checkoutPriceForCartWithTwoItems := cashier checkout: cartWithTwoItems 
	with: validCreditCard 
	on: aFixedDateOnThe3rdOfNovember2021 
	registeringOn: salesLog.
	
	checkoutPriceForCartWithOneItem := cashier checkout: cartWithOneItem
	with: validCreditCard 
	on: aFixedDateOnThe3rdOfNovember2021 
	registeringOn: salesLog.
	
	self assert: 2 equals: salesLog size.
	self assert: (salesLog includes: 'Checkout registered on ', aFixedDateOnThe3rdOfNovember2021 asString, ' for: ', checkoutPriceForCartWithTwoItems asString, '$').
	self assert: (salesLog includes: 'Checkout registered on ', aFixedDateOnThe3rdOfNovember2021 asString, ' for: ', checkoutPriceForCartWithOneItem asString, '$').! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:31:56'!
test09CheckingOutWithACreditCardWithNoFundsToMercahntProcessorShouldPromptCreditCardWithNoFundsError

	| cashier salesLog|

	salesLog := OrderedCollection new.
	cashier := Cashier new.
		
	self 
		should: [ cashier checkout: cartWithOneItem with: creditCardWithNoFunds on: aFixedDateOnThe3rdOfNovember2021 registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Credit Card was rejected, does not have money available' equals: anError messageText.
			self assert: salesLog isEmpty.]! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 15:32:26'!
test10WhenChekingOutWithAStolenCreditCardToMerchantProcessorShouldPromptStolenCreditCardError

	| cashier salesLog|
	
	salesLog := OrderedCollection new.
	cashier := Cashier new.
		
	self 
		should: [ cashier checkout: cartWithOneItem with: stolenCreditCard on: aFixedDateOnThe3rdOfNovember2021 registeringOn: salesLog]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Credit Card was marked as stolen, cannot complete checkout' equals: anError messageText.
			self assert: salesLog isEmpty.]! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:20:02'!
setUp

	testObjectsFactory := StoreTestObjectsFactory new.! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 14:51:24'!
test01WhenCreatingACreditCardWithAnInvalidIdShouldPromptInvalidIdError

	self
		should: [ CreditCard with: 'not-an-id' for: (GregorianMonthOfYear mayOf: 2021). ]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot create a Credit Card with an invalid ID' equals: anError messageText]
		! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 14:53:07'!
test02WhenCreatingACreditCardWithAnInvalidExpirationDateShouldPromptInvalidExpirationDateError

	self
		should: [ CreditCard with: 123 for: 'not a calendar'. ]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Cannot create a Credit Card with an invalid expiration date' equals: anError messageText]
		! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 14:53:55'!
test03WhenCreatingACreditCardWithAnIdWithLenghtNotEqualTo16ShouldPromptInvalidLenghtError

	self
		should: [ CreditCard with: 123 for:(GregorianMonthOfYear mayOf: 2021). ]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: 'Credit Card ID must have 16 digits' equals: anError messageText]
		! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 14:54:32'!
test04WhenCreatingAValidCartShouldNotGenerateErrors

	| creditCard |
	
	creditCard := testObjectsFactory validCreditCard .
	
	self assert: 1234123412341234 equals: creditCard numberId.
	self assert: (GregorianMonthOfYear yearNumber: 2022 monthNumber: 01) equals: creditCard expirationDate.! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'AR 11/3/2021 21:57:39'!
assertIsValidItem: anItem

	(catalog keys includes: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:03:21'!
price

	^ items sum: [:item | catalog at: item] ifEmpty: [0].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:20:58'!
creditCardHasNoFundsAvailableError
	
	self error: 'Credit Card was rejected, does not have money available'! !

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:22:56'!
creditCardIsStolenError
	
	self error: 'Credit Card was marked as stolen, cannot complete checkout'! !

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:47:49'!
emptyCartAtCheckoutError

	^ self error: 'Cannot checkout an empty cart'! !

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:48:43'!
expiredCreditCardError

	^ self error: 'Cannot checkout with an expired Credit Card'! !

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:48:17'!
invalidCartError

	^ self error: 'Cannot checkout something that is not a Cart'! !

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:49:07'!
invalidCreditCardError

	^ self error: 'Cannot checkout with something that isnt a Credit Card'! !

!Cashier methodsFor: 'error messages' stamp: 'ts 11/4/2021 14:49:21'!
invalidDateError

	^ self error: 'Cannot checkout on an invalid date'! !


!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:47:49'!
assertCartIsNotEmpty: aCart

	^ (aCart isEmpty) ifTrue: [self emptyCartAtCheckoutError]! !

!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:48:17'!
assertCartIsValid: aCart

	^ (aCart isKindOf: Cart) ifFalse: [self invalidCartError]! !

!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:37:52'!
assertCartIsValidForCheckout: aCart

	self assertCartIsValid: aCart.
	self assertCartIsNotEmpty: aCart! !

!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:48:43'!
assertCreditCard: aCreditCard isNotExpiredBy: aDate

	^ ((aDate monthOfYear) < (aCreditCard expirationDate)) ifFalse: [self expiredCreditCardError]! !

!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:49:07'!
assertCreditCardIsValid: aCreditCard

	(aCreditCard isKindOf: CreditCard ) ifFalse: [self invalidCreditCardError].
! !

!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:46:23'!
assertCreditCardIsValidForCheckout: aCreditCard for: aDate

	self assertCreditCardIsValid: aCreditCard .
	self assertCreditCard: aCreditCard isNotExpiredBy: aDate! !

!Cashier methodsFor: 'assertions' stamp: 'ts 11/4/2021 14:49:21'!
assertDateIsValid: aDate

	(aDate isKindOf: GregorianDate ) ifFalse: [self invalidDateError].! !


!Cashier methodsFor: 'initialization' stamp: 'ts 11/4/2021 14:17:58'!
initialize

	merchantProcessor := CashierToMerchantProcessorAdapterSimulator for: self.! !


!Cashier methodsFor: 'checkout' stamp: 'ts 11/4/2021 14:36:18'!
checkout: aCart with: aCreditCard on: aDate registeringOn: aSalesBook

	| checkoutPrice |

	self assertCartIsValidForCheckout: aCart.
	self assertDateIsValid: aDate.
	self assertCreditCardIsValidForCheckout: aCreditCard for: aDate.
	
	checkoutPrice := aCart price.
	
	self sendPaymentToMerchantProcessorWith: aCreditCard for: checkoutPrice.
	
	aSalesBook add: 'Checkout registered on ', aDate asString, ' for: ', checkoutPrice asString, '$' .
	
	^ checkoutPrice.! !


!Cashier methodsFor: 'merchant processor communication' stamp: 'AR 11/3/2021 23:48:29'!
sendPaymentToMerchantProcessorWith: aCreditCard for: checkoutPrice

	^ merchantProcessor pay: checkoutPrice with: aCreditCard .! !


!classDefinition: #CashierToMerchantProcessorAdapter category: 'TusLibros'!
Object subclass: #CashierToMerchantProcessorAdapter
	instanceVariableNames: 'adapteeCashier'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierToMerchantProcessorAdapter methodsFor: 'payment processing' stamp: 'AR 11/3/2021 23:49:44'!
pay: aPrice with: aCreditCard

	^ self subclassResponsibility .! !


!CashierToMerchantProcessorAdapter methodsFor: 'initialization' stamp: 'ts 11/4/2021 14:17:01'!
initializeFor: anAdapteeCashier

	^self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CashierToMerchantProcessorAdapter class' category: 'TusLibros'!
CashierToMerchantProcessorAdapter class
	instanceVariableNames: ''!

!CashierToMerchantProcessorAdapter class methodsFor: 'as yet unclassified' stamp: 'ts 11/4/2021 14:17:29'!
for: anAdapteeCashier

	^self new initializeFor: anAdapteeCashier! !


!classDefinition: #CashierToMerchantProcessorAdapterSimulator category: 'TusLibros'!
CashierToMerchantProcessorAdapter subclass: #CashierToMerchantProcessorAdapterSimulator
	instanceVariableNames: 'message1ReturnValue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierToMerchantProcessorAdapterSimulator methodsFor: 'testing' stamp: 'ts 11/4/2021 14:10:09'!
creditCardHasNoFundsAvailable: aCreditCard

	^ aCreditCard numberId = 1111111111111111! !

!CashierToMerchantProcessorAdapterSimulator methodsFor: 'testing' stamp: 'ts 11/4/2021 14:09:05'!
isCreditCardStolen: aCreditCard

	^ aCreditCard numberId = 6666666666666666! !


!CashierToMerchantProcessorAdapterSimulator methodsFor: 'error detection' stamp: 'ts 11/4/2021 14:20:29'!
assertCreditCardHasFundsAvailable: aCreditCard

	^ (self creditCardHasNoFundsAvailable: aCreditCard) ifTrue: [adapteeCashier creditCardHasNoFundsAvailableError]! !

!CashierToMerchantProcessorAdapterSimulator methodsFor: 'error detection' stamp: 'ts 11/4/2021 14:21:47'!
assertCreditCardIsNotStolen: aCreditCard

	^ (self isCreditCardStolen: aCreditCard)  ifTrue: [adapteeCashier creditCardIsStolenError]! !


!CashierToMerchantProcessorAdapterSimulator methodsFor: 'payment processing' stamp: 'ts 11/4/2021 14:11:48'!
pay: aPrice with: aCreditCard
	
	self assertCreditCardIsNotStolen: aCreditCard.
	self assertCreditCardHasFundsAvailable: aCreditCard.
	! !


!CashierToMerchantProcessorAdapterSimulator methodsFor: 'initialization' stamp: 'ts 11/4/2021 14:18:50'!
initializeFor: anAdapteeCashier

	adapteeCashier := anAdapteeCashier! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'numberId expirationDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'AR 11/3/2021 21:40:17'!
initializeWith: aNumberId for: anExpirationDate

	(aNumberId isKindOf: Integer) ifFalse: [self error: 'Cannot create a Credit Card with an invalid ID'].
	(anExpirationDate isKindOf: GregorianMonthOfYear) ifFalse: [self error: 'Cannot create a Credit Card with an invalid expiration date'].
	((aNumberId numberOfDigitsInBase: 10) = 16) ifFalse: [self error: 'Credit Card ID must have 16 digits'].
	
	numberId := aNumberId .
	expirationDate := anExpirationDate .! !


!CreditCard methodsFor: 'accesing' stamp: 'AR 11/1/2021 20:09:26'!
expirationDate

	^ expirationDate! !

!CreditCard methodsFor: 'accesing' stamp: 'AR 11/1/2021 20:09:11'!
numberId

	^ numberId! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'as yet unclassified' stamp: 'AR 11/1/2021 19:58:14'!
with: aNumberId for: anExpirationDate

	^ self new initializeWith: aNumberId for: anExpirationDate.! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'stolenCreditCard'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:15:31'!
createCart
	
	^ Cart acceptingItemsOf: self defaultCatalog.! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/4/2021 00:07:15'!
creditCardWithoutFunds

	^ CreditCard with: 1111111111111111 for: (GregorianMonthOfYear yearNumber: 2022 monthNumber: 01).! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:26:24'!
dateAfterExpiration

	^ FixedGregorianDate yearNumber: 2020 monthNumber: 10 dayNumber: 3.! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:19:35'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice; yourself.! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:29:24'!
expiredCreditCard

	^ CreditCard with: 1234123412341234 for: (GregorianMonthOfYear yearNumber: 2019 monthNumber: 08).

	! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:13:52'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:19:35'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:15:18'!
itemSellByTheStorePrice
	
	^ 10.! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 23:42:09'!
stolenCreditCard

	^ CreditCard with: 6666666666666666 for: (GregorianMonthOfYear yearNumber: 2023 monthNumber: 09).! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'AR 11/3/2021 22:29:05'!
validCreditCard

	^ CreditCard with: 1234123412341234 for: (GregorianMonthOfYear yearNumber: 2022 monthNumber: 01).! !
