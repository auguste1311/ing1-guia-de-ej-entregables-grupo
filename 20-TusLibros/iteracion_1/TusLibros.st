!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:48:14'!
test01AfterCreatingAShoppingCartItShouldBeEmpty

	|aShoppingCart |
	
	aShoppingCart _ ShoppingCart for: (OrderedCollection new).
	
	self assert: aShoppingCart isEmpty.
	self assert: 0 equals: aShoppingCart cartSize .
 
	! !

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:42:29'!
test02CantAddContentsThatAreNotBookTitles

	| shoppingCart catalog |
	
	catalog := OrderedCollection with: 'Harry Potter' with: 'Toda Mafalda'.
	shoppingCart := ShoppingCart for: catalog.
	
	self should: [shoppingCart add: 42 times: 1]
		raise: Error
		withExceptionDo: [ :anError | 
			self assert:  'Cannot add contents that are not book titles' equals: anError messageText.
			self assert: shoppingCart isEmpty ]! !

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:43:27'!
test03CantAddBooksThatDontBelongToTheCatalog
	| shoppingCart catalog |
	
	catalog := OrderedCollection with: 'Harry Potter' with: 'Toda Mafalda'.
	shoppingCart := ShoppingCart for: catalog.
	
	self should: [shoppingCart add: 'El Sabio de la Tribu' times: 4]
		raise: Error
		withExceptionDo: [ :anError | 
			self assert:  'Cannot add a book that doesnt belong to the catalog' equals: anError messageText.
			self assert: shoppingCart isEmpty ]! !

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:49:09'!
test04CanAddBooksThatBelongToTheCatalog

	| shoppingCart catalog |
	
	catalog := OrderedCollection with: 'Harry Potter' with: 'Toda Mafalda'.
	shoppingCart := ShoppingCart for: catalog.	
	
	shoppingCart add: 'Harry Potter' times: 2.
	
	self assert: (shoppingCart hasOrdered: 'Harry Potter').
	self deny: (shoppingCart hasOrdered: 'Toda Mafalda').
	self assert: 2 equals: shoppingCart cartSize .! !

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:44:10'!
test05AmountOrderedMustBeGreaterThanZero

	| shoppingCart catalog |
	
	catalog := OrderedCollection with: 'Harry Potter' with: 'Toda Mafalda'.
	shoppingCart := ShoppingCart for: catalog.
	
	self should: [shoppingCart add: 'Harry Potter' times: 0]
		raise: Error
		withExceptionDo: [ :anError | 
			self assert:  'Amount of books ordered must be greater than zero' equals: anError messageText.
			self assert: shoppingCart isEmpty ]! !

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:44:20'!
test06AddingMultipleBooksShouldRaiseTheCartSize

	| shoppingCart catalog |
	
	catalog := OrderedCollection with: 'Harry Potter' with: 'Toda Mafalda'.
	shoppingCart := ShoppingCart for: catalog.	
	
	shoppingCart add: 'Harry Potter' times: 4.
	shoppingCart add: 'Toda Mafalda' times: 3.
	
	self assert: 7 equals: shoppingCart cartSize.! !

!TusLibrosTest methodsFor: 'shopping cart tests' stamp: 'AR 11/1/2021 14:49:38'!
test07AddingTheSameBookMultiplesIncreasesItsOrderedAmount

	| shoppingCart catalog |
	
	catalog := OrderedCollection with: 'Harry Potter' with: 'Toda Mafalda'.
	shoppingCart := ShoppingCart for: catalog.	
	
	shoppingCart add: 'Harry Potter' times: 4.
	shoppingCart add: 'Harry Potter' times: 3.
		
	self assert: 7 equals: (shoppingCart orderedAmount: 'Harry Potter').! !


!classDefinition: #ShoppingCart category: 'TusLibros'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'contents catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ShoppingCart methodsFor: 'assertions' stamp: 'AR 11/1/2021 14:37:08'!
assertIsABookTitle: aBookTitle

	(aBookTitle isKindOf: String) ifFalse: [self error: 'Cannot add contents that are not book titles']! !

!ShoppingCart methodsFor: 'assertions' stamp: 'AR 11/1/2021 14:38:21'!
assertIsAValidQuantity: aQuantity

	(aQuantity > 0) ifFalse: [self error: 'Amount of books ordered must be greater than zero']! !

!ShoppingCart methodsFor: 'assertions' stamp: 'AR 11/1/2021 13:47:56'!
assertTitleBelongsInCatalog: aBookTitle

	 (catalog includes: aBookTitle) ifFalse: [self error: 'Cannot add a book that doesnt belong to the catalog'].! !

!ShoppingCart methodsFor: 'assertions' stamp: 'AR 11/1/2021 15:00:14'!
assertValidOrderFor: aBookTitle times: aQuantity

	self assertIsABookTitle: aBookTitle.
	self assertTitleBelongsInCatalog: aBookTitle.
	self assertIsAValidQuantity: aQuantity.! !


!ShoppingCart methodsFor: 'cart size operations' stamp: 'AR 11/1/2021 14:57:31'!
cartSize
	^ contents values inject: 0
		into: [ :currentCartSize :aBookAmount | aBookAmount + currentCartSize ]! !

!ShoppingCart methodsFor: 'cart size operations' stamp: 'AR 10/31/2021 23:03:56'!
isEmpty

	^ contents isEmpty.! !


!ShoppingCart methodsFor: 'initialization' stamp: 'AR 10/31/2021 23:09:29'!
initializeFor: aCatalog

	contents _ Dictionary new.
	catalog := aCatalog.! !


!ShoppingCart methodsFor: 'addition' stamp: 'AR 11/1/2021 15:00:04'!
add: aBookTitle times: aQuantity

	| timesOrdered |
	self assertValidOrderFor: aBookTitle times: aQuantity .
	
	timesOrdered := contents at: aBookTitle ifAbsent: [0].
	contents at: aBookTitle put: timesOrdered + aQuantity.
	
	! !


!ShoppingCart methodsFor: 'testing' stamp: 'AR 10/31/2021 23:39:42'!
hasOrdered: aBookTitle
	
	^ contents includesKey: aBookTitle .! !

!ShoppingCart methodsFor: 'testing' stamp: 'AR 11/1/2021 14:50:24'!
orderedAmount: aBookTitle

	^ contents at: aBookTitle ifAbsent: [^0].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: 'TusLibros'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'as yet unclassified' stamp: 'AR 10/31/2021 22:54:20'!
for: aCatalog

	^ self new initializeFor: aCatalog .! !
