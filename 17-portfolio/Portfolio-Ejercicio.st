!classDefinition: #PortfolioTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:42:14'!
test01ANewPortfolioHasNoBalance

	|portfolio|
	
	portfolio _ Portfolio new.
	
	self assert: 0 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:42:46'!
test02APortfolioWithAnEmptyAccountHasNoBalance

	|portfolio receptiveAccount|
	
	portfolio _ Portfolio new.
	
	receptiveAccount _ ReceptiveAccount new.
	
	portfolio add: receptiveAccount.
	
	self assert: 0 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:43:50'!
test03APortfolioWithAnAccountOfBalance200HasBalance200

	|portfolio |
	
	portfolio _ Portfolio new.
	
	self addAnAccountWithADepositOf: 200 on: portfolio.
	
	self assert: 200 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:45:35'!
test04APortfolioWith2AccountsHaveABalanceEqualToTheSumOfTheAccountsBalance

	|portfolio receptiveAccountWithAWithdraw|
	
	portfolio _ Portfolio new.
	
	self addAnAccountWithADepositOf: 200 on: portfolio.
	
	receptiveAccountWithAWithdraw _ ReceptiveAccount new.
	
	AccountTransaction withdraw: 100 from: receptiveAccountWithAWithdraw.
	
	portfolio add: receptiveAccountWithAWithdraw.
	
	self assert: 100 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:46:12'!
test05APortfolioWithAnEmptyPortfolioHasNoBalance

	|mainPortfolio secondaryPortfolio|
	
	mainPortfolio _ Portfolio new.
	
	secondaryPortfolio _ Portfolio new.
	
	mainPortfolio add: secondaryPortfolio.
	
	self assert: 0 equals: mainPortfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:46:47'!
test06APortfolioWithAPortfolioOfBalance200HasBalance200

	|portfolio|
	
	portfolio _ Portfolio new.
	
	self addAPortfolioWithABalanceOf: 200 to: portfolio.
	
	self assert: 200 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:47:37'!
test07APortfolioWith2PortfoliosHaveABalanceEqualToTheSumOfThePortfoliosBalance

	|portfolio|
	
	portfolio _ Portfolio new.
	
	self addAPortfolioWithABalanceOf: 200 to: portfolio.
	
	self addAPortfolioWithABalanceOf: 200 to: portfolio.
	
	self assert: 400 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:48:52'!
test08APortfolioWithAnAccountAndAPortfolioHasBalanceEqualToTheSumOfTheirBalances

	|portfolio|
	
	portfolio _ Portfolio new.
	
	self addAnAccountWithADepositOf: 200 on: portfolio.
	
	self addAPortfolioWithABalanceOf: 200 to: portfolio.
	
	self assert: 400 equals: portfolio balance! !

!PortfolioTest methodsFor: 'balance tests' stamp: 'ts 10/18/2021 12:50:50'!
test09BalancesAreTransitiveWithinAPortfolioTree

	|aSmallPortfolio aBigPortfolio|
	
	aSmallPortfolio _ Portfolio new.
	
	self addAnAccountWithADepositOf: 200 on: aSmallPortfolio.
	
	self addAPortfolioWithABalanceOf: 200 to: aSmallPortfolio.
	
	aBigPortfolio _ Portfolio new.
	
	aBigPortfolio add: aSmallPortfolio.
	
	self assert: 400 equals: aBigPortfolio balance! !


!PortfolioTest methodsFor: 'transactions tests' stamp: 'ts 10/18/2021 12:53:09'!
test10ANewPortfolioHasNoTransactions

	|portfolio|
	
	portfolio _ Portfolio new.
	
	self assert: portfolio transactions isEmpty.! !

!PortfolioTest methodsFor: 'transactions tests' stamp: 'ts 10/18/2021 12:53:47'!
test11APortfolioWithAnAccountHasThatAccountTransactions

	|portfolio aReceptiveAccount|
	
	portfolio _ Portfolio new.
	
	aReceptiveAccount  _ ReceptiveAccount new.
	
	AccountTransaction deposit: 200 on: aReceptiveAccount .
	
	portfolio add: aReceptiveAccount.
	
	self assert: aReceptiveAccount transactions equals: portfolio transactions.! !

!PortfolioTest methodsFor: 'transactions tests' stamp: 'ts 10/18/2021 12:54:20'!
test12APortfolioWithAPortfolioHasThatPortfolioTransactions

	|mainPortfolio secondaryPortfolio |
	
	secondaryPortfolio _ self createAPortfolioWithAnAccountOfA200Deposit .
	
	mainPortfolio _ Portfolio new.
	
	mainPortfolio add: secondaryPortfolio.
	
	self assert: secondaryPortfolio transactions equals: mainPortfolio transactions.! !

!PortfolioTest methodsFor: 'transactions tests' stamp: 'ts 10/18/2021 12:56:23'!
test13AportfolioHasAllTheTransactionsOfItsoffspring

	|mainPortfolio secondaryPortfolio aReceptiveAccount expectedMainPortfolioTransactions |
	
	mainPortfolio _ Portfolio new.
	
	aReceptiveAccount _ self createAReceptiveAccountWithAnAccountOfA200Deposit.
	mainPortfolio add: aReceptiveAccount.
	
	secondaryPortfolio _ self createAPortfolioWithAnAccountOfA200Deposit.
	mainPortfolio add: secondaryPortfolio.
	
	expectedMainPortfolioTransactions _ (aReceptiveAccount transactions addAll: 
		secondaryPortfolio transactions).
	
	self assert: expectedMainPortfolioTransactions equals: mainPortfolio transactions.
	! !


!PortfolioTest methodsFor: 'loop detection tests' stamp: 'ts 10/18/2021 13:01:43'!
test18CannotAddTheSameAccountTwiceToAPortfolio

	| portfolio anAccount  |
	
	portfolio _ Portfolio new.
	anAccount _ ReceptiveAccount new.
	
	portfolio add: anAccount .
	
	self should: [portfolio add: anAccount ]
        		raise: Error
       	 	withMessageText: 'Cannot register an account that already exists in the portfolio'

! !

!PortfolioTest methodsFor: 'loop detection tests' stamp: 'ts 10/18/2021 13:02:11'!
test19CannotAddAnAccountThatAlreadyExistsInThePortfolioOffspring

	| portfolio anotherPortfolio anAccount  |
	
	portfolio _ Portfolio new.
	anotherPortfolio _ Portfolio new.
	anAccount _ ReceptiveAccount new.
	
	portfolio add: anotherPortfolio .
	anotherPortfolio add: anAccount .
	
	self should: [portfolio add: anAccount]
        		raise: Error
       	 	withMessageText: 'Cannot register an account that already exists in the portfolio'

! !

!PortfolioTest methodsFor: 'loop detection tests' stamp: 'ts 10/18/2021 13:02:40'!
test20CannotAddTheSAmePortfolioTwiceToAPortfolio

	| portfolio anotherPortfolio |
	
	portfolio _ Portfolio new.
	anotherPortfolio _ Portfolio new.
	
	portfolio add: anotherPortfolio .
	
	self should: [portfolio add: anotherPortfolio]
        		raise: Error
       	 	withMessageText: 'Cannot register a portfolio that already exists in the portfolio'

! !

!PortfolioTest methodsFor: 'loop detection tests' stamp: 'ts 10/18/2021 13:03:09'!
test21CannotAddAPortfolioThatAlreadyExistsInItsOffspring

	| mainPortfolio secondaryPortfolio terciaryPortfolio  |
	
	mainPortfolio _ Portfolio new.
	secondaryPortfolio _ Portfolio new.
	terciaryPortfolio _ Portfolio new.
	
	mainPortfolio add: secondaryPortfolio .
	secondaryPortfolio add: terciaryPortfolio .
	
	self should: [mainPortfolio add: terciaryPortfolio]
        		raise: Error
       	 	withMessageText: 'Cannot register a portfolio that already exists in the portfolio'

! !

!PortfolioTest methodsFor: 'loop detection tests' stamp: 'ts 10/18/2021 13:54:16'!
test22CannotAddAnAccountThatAlreadyExistsInTheTree

	|mainPortfolio secondaryPortfolio aReceptiveAccount|
	
	aReceptiveAccount _ self createAReceptiveAccountWithAnAccountOfA200Deposit.
	
	mainPortfolio _ Portfolio new.
	
	secondaryPortfolio _ Portfolio new.
	
	mainPortfolio add: aReceptiveAccount .
	mainPortfolio add: secondaryPortfolio .
	
	self should: [secondaryPortfolio add: aReceptiveAccount]
		raise: Error
		withMessageText: 'Cannot add an account on a portfolio that already have it in its underlying tree'! !

!PortfolioTest methodsFor: 'loop detection tests' stamp: 'AR 10/18/2021 16:09:18'!
test23CannotAddAPortfolioThatAlreadyExistsInTheTree

	|rootPorfolio firstChildPortfolio secondChildPortfolio|
	
	rootPorfolio _ Portfolio new.
	
	firstChildPortfolio _ Portfolio new.
	
	secondChildPortfolio _ Portfolio new.
	
	rootPorfolio add: firstChildPortfolio .
	rootPorfolio add: secondChildPortfolio .
	
	self should: [firstChildPortfolio add: secondChildPortfolio]
		raise: Error
		withMessageText: 'Cannot add a portfolio on a portfolio that already have it in its underlying tree'! !


!PortfolioTest methodsFor: 'registered transactions test' stamp: 'ts 10/18/2021 12:57:33'!
test14ANewPortfolioDoesntHaveRegisteredAnyTransaction

	|portfolio aTransaction|
	
	portfolio _ Portfolio new.
	
	aTransaction _ AccountTransaction new initializeNewDepositOf: 300.
	
	self deny: (portfolio hasRegistered: aTransaction). ! !

!PortfolioTest methodsFor: 'registered transactions test' stamp: 'ts 10/18/2021 12:58:50'!
test15APortfolioWithAnAccountHasRegisteredTheTransactionsOfThatAccount

	|portfolio aTransaction aReceptiveAccount|
	
	portfolio _ Portfolio new.
	
	aReceptiveAccount _ ReceptiveAccount new.
	
	aTransaction _ AccountTransaction new initializeNewDepositOf: 300.
	
	aReceptiveAccount register: aTransaction.
	
	portfolio add: aReceptiveAccount.
	
	self assert: (portfolio hasRegistered: aTransaction). ! !

!PortfolioTest methodsFor: 'registered transactions test' stamp: 'ts 10/18/2021 12:59:25'!
test16APortfolioWithAPortfolioHasRegisteredTheTransactionsOfThatPortfolio

	|mainPortfolio secondaryPortfolio aTransaction aReceptiveAccount|
	
	secondaryPortfolio _ Portfolio new.
	
	aReceptiveAccount _ ReceptiveAccount new.
	
	aTransaction _ AccountTransaction new initializeNewDepositOf: 300.
	
	aReceptiveAccount register: aTransaction.
	
	secondaryPortfolio add: aReceptiveAccount.
	
	mainPortfolio _ Portfolio new.
	
	mainPortfolio add: secondaryPortfolio.
	
	self assert: (mainPortfolio hasRegistered: aTransaction). ! !

!PortfolioTest methodsFor: 'registered transactions test' stamp: 'ts 10/18/2021 12:59:53'!
test17ATransactionCannotBeRegisteredDirectlyToAPortfolio

	|portfolio aTransaction|
	
	portfolio _ Portfolio new.
	
	aTransaction _ AccountTransaction new initializeNewDepositOf: 300.
	
	self should: [portfolio register: aTransaction]
		raise: Error
		withMessageText: 'Cannot register a transaction on a portfolio'

! !


!PortfolioTest methodsFor: 'auxiliary methods' stamp: 'ts 10/17/2021 23:21:40'!
addAPortfolioWithABalanceOf: aBalanceValue to: aPortfolio

	|secondaryPortfolio|
	
	secondaryPortfolio _ Portfolio new.
	
	self addAnAccountWithADepositOf: 200 on: secondaryPortfolio.
	
	aPortfolio add: secondaryPortfolio.! !

!PortfolioTest methodsFor: 'auxiliary methods' stamp: 'ts 10/17/2021 23:21:47'!
addAnAccountWithADepositOf: aDepositValue on: aPortfolio

	|receptiveAccount|
	
	receptiveAccount _ ReceptiveAccount new.
	
	AccountTransaction deposit: aDepositValue on: receptiveAccount.
	
	aPortfolio add: receptiveAccount.! !

!PortfolioTest methodsFor: 'auxiliary methods' stamp: 'ts 10/17/2021 23:54:11'!
createAPortfolioWithAnAccountOfA200Deposit

	|portfolio aReceptiveAccount|
	
	portfolio _ Portfolio new.
	
	aReceptiveAccount  _ ReceptiveAccount new.
	
	AccountTransaction deposit: 200 on: aReceptiveAccount .
	
	portfolio add: aReceptiveAccount.
	
	^portfolio ! !

!PortfolioTest methodsFor: 'auxiliary methods' stamp: 'ts 10/18/2021 00:02:48'!
createAReceptiveAccountWithAnAccountOfA200Deposit
	
	|aReceptiveAccount|
	
	aReceptiveAccount _ ReceptiveAccount new.
	
	AccountTransaction deposit: 200 on: aReceptiveAccount .
	
	^aReceptiveAccount! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'ts 10/17/2021 21:49:24'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	AccountTransaction deposit: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'ts 10/17/2021 21:50:30'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	AccountTransaction deposit: 100 on: account.
	AccountTransaction withdraw: 50 from: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'ts 10/17/2021 21:57:49'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (AccountTransaction withdraw: withdrawValue from: account) withdrawValue
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'ts 10/17/2021 22:03:22'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := AccountTransaction deposit:100 on: account.
	withdraw := AccountTransaction withdraw: 50 from: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'ts 10/17/2021 22:15:06'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  AccountTransaction new initializeNewDepositOf: 200.
	withdraw := AccountTransaction new initializeNewWithdrawOf: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'ts 10/17/2021 22:06:17'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := AccountTransaction deposit: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: 'depositValue withdrawValue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'initialization' stamp: 'ts 10/17/2021 22:12:51'!
initializeNewDepositOf: aValue 
	depositValue _ aValue.
	withdrawValue _ 0! !

!AccountTransaction methodsFor: 'initialization' stamp: 'ts 10/17/2021 22:13:02'!
initializeNewWithdrawOf: aValue 
	withdrawValue _ aValue.
	depositValue _ 0! !


!AccountTransaction methodsFor: 'value' stamp: 'ts 10/17/2021 22:09:12'!
depositValue

	^depositValue! !

!AccountTransaction methodsFor: 'value' stamp: 'ts 10/17/2021 22:09:08'!
withdrawValue

	^withdrawValue! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'ts 10/17/2021 21:54:40'!
deposit: aValue on: account

	| transaction |
	
	transaction := self new initializeNewDepositOf: aValue.
	account register: transaction.
		
	^ transaction! !

!AccountTransaction class methodsFor: 'instance creation' stamp: 'ts 10/17/2021 21:55:36'!
withdraw: aValue from: account

	| transaction |
	
	transaction := self new initializeNewWithdrawOf: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #PortfolioEntry category: 'Portfolio-Ejercicio'!
Object subclass: #PortfolioEntry
	instanceVariableNames: 'parentPortfolios'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioEntry methodsFor: 'transactions' stamp: 'ts 10/18/2021 00:50:24'!
hasRegistered: anAccountTransaction

	^self subclassResponsibility .! !

!PortfolioEntry methodsFor: 'transactions' stamp: 'ts 10/17/2021 23:39:17'!
transactions

	^self subclassResponsibility ! !


!PortfolioEntry methodsFor: 'balance' stamp: 'ts 10/17/2021 23:24:00'!
balance

	^self subclassResponsibility ! !


!PortfolioEntry methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:19:04'!
hasOrIsAccount: aReceptiveAccount

	^self subclassResponsibility ! !

!PortfolioEntry methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:31:32'!
hasOrIsPortfolio: aPortfolio

	^self subclassResponsibility ! !


!PortfolioEntry methodsFor: 'adding assertions' stamp: 'ts 10/18/2021 12:31:48'!
assertCanBeAddedTo: aPortfolio

	^self subclassResponsibility ! !


!PortfolioEntry methodsFor: 'as yet unclassified' stamp: 'ts 10/18/2021 13:58:54'!
assignParent: aPortfolio

	parentPortfolios add: aPortfolio ! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
PortfolioEntry subclass: #Portfolio
	instanceVariableNames: 'registeredPortfolioEntries'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'balance' stamp: 'ts 10/17/2021 23:10:47'!
balance

	^ registeredPortfolioEntries sum: [:aPortfolioEntry | aPortfolioEntry balance] ifEmpty: [0]! !


!Portfolio methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:20:49'!
hasAccount: aReceptiveAccount

	|isAccountRegistered|
	
	isAccountRegistered _ false.
	
	registeredPortfolioEntries do: [:aRegisteredEntry | 
		isAccountRegistered _ isAccountRegistered or: [aRegisteredEntry hasOrIsAccount: aReceptiveAccount]
	].
	
	^isAccountRegistered! !

!Portfolio methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:19:48'!
hasOrIsAccount: aReceptiveAccount

	^self hasAccount: aReceptiveAccount! !

!Portfolio methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:27:55'!
hasOrIsPortfolio: aPortfolio

	^self = aPortfolio or: [self hasPortfolio: aPortfolio]! !

!Portfolio methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:25:53'!
hasPortfolio: aPortfolio

	|isPortfolioRegistered|
	
	isPortfolioRegistered _ false.
	
	registeredPortfolioEntries do: [:aRegisteredEntry | 
		isPortfolioRegistered _ isPortfolioRegistered or: [aRegisteredEntry hasOrIsPortfolio: aPortfolio]
	].
	
	^isPortfolioRegistered! !


!Portfolio methodsFor: 'initialization' stamp: 'ts 10/18/2021 14:01:30'!
initialize

	registeredPortfolioEntries _ OrderedCollection new.
	parentPortfolios _ OrderedCollection new.! !


!Portfolio methodsFor: 'adding' stamp: 'ts 10/18/2021 13:22:37'!
add: aPortfolioEntry

	self assertCanAdd: aPortfolioEntry.
	
	aPortfolioEntry assignParent: self.

	registeredPortfolioEntries add: aPortfolioEntry
	
	! !

!Portfolio methodsFor: 'adding' stamp: 'ts 10/18/2021 00:59:16'!
register: aTransaction

	self error: 'Cannot register a transaction on a portfolio'! !


!Portfolio methodsFor: 'adding assertions' stamp: 'ts 10/18/2021 13:17:05'!
assertCanAdd: aPortfolioEntry

	aPortfolioEntry assertCanBeAddedTo: self.
		! !

!Portfolio methodsFor: 'adding assertions' stamp: 'ts 10/18/2021 14:01:09'!
assertCanAddAccount: aReceptiveAccount

	(self hasAccount: aReceptiveAccount) ifTrue: 
		[self error: 'Cannot register an account that already exists in the portfolio'].
	
	parentPortfolios do: [:aParentPortfolioEntry |
			[aReceptiveAccount assertCanBeAddedTo: aParentPortfolioEntry]
			on: Error 
			do: [self error: 'Cannot add an account on a portfolio that already have it in its underlying tree']]! !

!Portfolio methodsFor: 'adding assertions' stamp: 'ts 10/18/2021 15:02:30'!
assertCanAddPortfolio: aPortfolio

	(self hasPortfolio: aPortfolio) ifTrue: [self error: 'Cannot register a portfolio that already exists in the portfolio'].
	
	parentPortfolios do: [:aParentPortfolioEntry |
			[aPortfolio assertCanBeAddedTo: aParentPortfolioEntry.]
				on: Error 
				do: [self error: 'Cannot add a portfolio on a portfolio that already have it in its underlying tree']
	]! !

!Portfolio methodsFor: 'adding assertions' stamp: 'AR 10/18/2021 16:06:37'!
assertCanBeAddedTo: aPortfolio

	aPortfolio assertCanAddPortfolio: self.
	
	self assertCanAddPortfolio: aPortfolio.! !


!Portfolio methodsFor: 'transactions' stamp: 'ts 10/18/2021 00:44:50'!
hasRegistered: aTransaction 
	
	^(registeredPortfolioEntries select: [:aPortfolioEntry | aPortfolioEntry hasRegistered: aTransaction]) isEmpty not.! !

!Portfolio methodsFor: 'transactions' stamp: 'ts 10/18/2021 00:51:17'!
transactions
	
	|portfolioTransactions|
	
	portfolioTransactions _ OrderedCollection new.
	
	registeredPortfolioEntries do: [:aPortfolioEntry | portfolioTransactions _ portfolioTransactions addAll: aPortfolioEntry transactions].
	
	^portfolioTransactions! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
PortfolioEntry subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'ts 10/18/2021 14:01:52'!
initialize

	transactions := OrderedCollection new.
	parentPortfolios _ OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'ts 10/17/2021 21:48:40'!
balance

	^ self depositBalance - self withdrawBalance! !

!ReceptiveAccount methodsFor: 'balance' stamp: 'ts 10/17/2021 21:55:55'!
depositBalance

	^ transactions sum: [:aTransaction | aTransaction depositValue] ifEmpty: [0]! !

!ReceptiveAccount methodsFor: 'balance' stamp: 'ts 10/17/2021 22:07:00'!
withdrawBalance

	^ transactions sum: [:aTransaction | aTransaction withdrawValue] ifEmpty: [0]! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'adding assertions' stamp: 'ts 10/18/2021 12:33:06'!
assertCanBeAddedTo: aPortfolio

	aPortfolio assertCanAddAccount: self! !


!ReceptiveAccount methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:20:33'!
hasOrIsAccount: aReceptiveAccount

	^self = aReceptiveAccount

	! !

!ReceptiveAccount methodsFor: 'entry detection' stamp: 'ts 10/18/2021 12:27:01'!
hasOrIsPortfolio: aPortfolio

	^false

	! !
