!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'wintPrefix winterIsComingSentence winterIsHereSentence winterIsGoneSentence summerIsHereSentence summerIsComingSentence summerIsGoneSentence'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:02:04'!
test01FinderWithAnEmptyStackReturnsEmptyCollection

	| emptyStack finder results |
	
	emptyStack := OOStack new.
	finder := SentenceFinderByPrefix on: emptyStack.
	
	results := (finder find: 'Ingenieria').
	
	self assert: results isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:02:08'!
test02FinderCanMatchPrefixOnASingleSentence

	| stack finder result |
	
	stack := OOStack new.	
	stack push: winterIsComingSentence.
	
	finder := SentenceFinderByPrefix on: stack.
	
	result := finder find: wintPrefix.
	
	self assert: result includes: winterIsComingSentence .
	self assert: result size = 1.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:03:13'!
test03FinderCanMatchPrefixOnMultipleSentencesAllContainingPrefix

	| stack finder result  |
	
	stack := OOStack new.
	
	stack push: winterIsComingSentence.
	stack push: winterIsHereSentence.
	stack push: winterIsGoneSentence.
	
	finder := SentenceFinderByPrefix on: stack.
	
	result := finder find: wintPrefix.
	
	self assert: result includes: winterIsComingSentence .
	self assert: result includes: winterIsHereSentence .
	self assert: result includes: winterIsGoneSentence .
	self assert: result size = 3.

	
! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:04:36'!
test04FinderCanMatchPrefixOnMultipleSentencesSomeContainingPrefix

	| stack finder result |
	
	stack := OOStack new.
	
	stack push: winterIsComingSentence.
	stack push: summerIsHereSentence.
	stack push: winterIsGoneSentence.
	
	finder := SentenceFinderByPrefix on: stack.
	result := finder find: wintPrefix.
	
	self deny: (result includes: summerIsHereSentence) .
	self assert: result size = 2.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:05:20'!
test05FinderCantMatchPrefixOnASingleSentenceWithoutThePrefix

	| stack finder result |
	
	stack := OOStack new.
	
	wintPrefix := 'Wint'.
	summerIsComingSentence := 	 'Summer is Coming'.
	
	stack push: summerIsComingSentence.
	finder := SentenceFinderByPrefix on: stack.
	
	result := finder find: wintPrefix.
	
	self deny: (result includes: summerIsComingSentence) .
	self assert: result size = 0.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:07:02'!
test06FinderCantMatchPrefixOnMultipleSentencesNotContainingPrefix

	| stack finder result |
	
	stack := OOStack new.
	
	stack push: summerIsComingSentence.
	stack push: summerIsHereSentence.
	stack push: summerIsGoneSentence.
	
	finder := SentenceFinderByPrefix on: stack.
	
	result := finder find: wintPrefix.
	
	self deny: (result includes: summerIsComingSentence) .
	self deny: (result includes: summerIsHereSentence) .
	self deny: (result includes: summerIsGoneSentence) .
	self assert: result size = 0.
! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:11:35'!
test07FinderDoesntModifyItsStackContentsAfterFinding

	| stack firstResult secondResult finder |
	
	stack := OOStack new.
	
	stack push: winterIsComingSentence.
	stack push: summerIsHereSentence.
	stack push: winterIsGoneSentence.
	
	finder := SentenceFinderByPrefix on: stack.
	firstResult := finder find: wintPrefix.
	secondResult := finder find: wintPrefix.
	
	self assert: firstResult = secondResult.
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:39:47'!
test08FinderReturnsAnErrorWhenBeingCreatedWithSomethingDifferentFromAStack

	self assertFailureOnExecuting: 	[SentenceFinderByPrefix on: 2.]  
	hasResult: SentenceFinderByPrefix errorASentenceFinderMustBeInitializedWithAStack.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:39:55'!
test09FinderReturnsAnErrorWhenBeingCreatedWithContentsThatAreNotString

	|invalidStack|
	
	invalidStack := OOStack new.
	invalidStack push: 42.

	self assertFailureOnExecuting: 	[SentenceFinderByPrefix on: invalidStack .]  
	hasResult: SentenceFinderByPrefix errorStackMustOnlyContainStrings .! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:40:03'!
test10FinderReturnsAnErrorWhenPrefixIsNotAString

	|stack finder|
	
	stack := OOStack new.
	stack push: winterIsComingSentence .
	
	finder := SentenceFinderByPrefix on: stack.
	
	self assertFailureOnExecuting: 	[finder find: 3 .]  
	hasResult: SentenceFinderByPrefix errorPrefixMustBeAString .! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:40:11'!
test11FinderReturnsAnErrorWhenPrefixIsEmpty

	|stack finder|
	
	stack := OOStack new.
	stack push: winterIsComingSentence .
	
	finder := SentenceFinderByPrefix on: stack.
	
	self assertFailureOnExecuting: 	[finder find: '' .]  
	hasResult: SentenceFinderByPrefix errorPrefixCantBeEmpty .! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AR 9/20/2021 15:40:18'!
test12FinderReturnsAnErrorWhenPrefixContainsWhitespaces

	|stack finder|
	
	stack := OOStack new.
	stack push: winterIsComingSentence .
	
	finder := SentenceFinderByPrefix on: stack.
	
	self assertFailureOnExecuting: 	[finder find: '  he  llo ' .]  
	hasResult: SentenceFinderByPrefix errorPrefixCantContainWhitespaces .! !


!SentenceFinderByPrefixTest methodsFor: 'setup' stamp: 'AR 9/20/2021 14:58:27'!
setUp

	"emptyStack := OOStack new.
	stackWithAllSentencesStartingWithWint := OOStack new.
	stackWithSomeSentencesStartingWithWint := OOStack new.
	stackWithNoSentencesStartingWithWint := OOStack new.
		
	emptyFinder := SentenceFinderByPrefix with: OOStack new."
	
	wintPrefix := 'Wint'.
	
	winterIsComingSentence := 	 'Winter is Coming'.
	winterIsHereSentence := 'Winter is Here'.
	winterIsGoneSentence := 'Winter is finally Gone'.
	
	summerIsHereSentence := 'Summer is Here'.
	summerIsComingSentence := 	 'Summer is Coming'.
	summerIsGoneSentence := 'Summer is finally Gone'.


! !


!SentenceFinderByPrefixTest methodsFor: 'utils' stamp: 'AR 9/20/2021 15:36:11'!
assertFailureOnExecuting: aClosure hasResult: anErrorDescription
	aClosure
		on: Error
		do: [:anError | self assert: anError messageText = anErrorDescription].! !


!classDefinition: #BaseStacker category: 'Stack-Exercise'!
Object subclass: #BaseStacker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!BaseStacker methodsFor: 'operations' stamp: 'TS 9/18/2021 22:37:36'!
stackedObject

	self error: OOStack stackEmptyErrorDescription! !


!classDefinition: #MiddleStacker category: 'Stack-Exercise'!
Object subclass: #MiddleStacker
	instanceVariableNames: 'stackedObject stackedOver'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!MiddleStacker methodsFor: 'operations' stamp: 'TS 9/18/2021 22:12:52'!
stackedObject

	^stackedObject ! !

!MiddleStacker methodsFor: 'operations' stamp: 'TS 9/18/2021 22:13:26'!
stackedOver

	^stackedOver! !


!MiddleStacker methodsFor: 'initialization' stamp: 'TS 9/18/2021 22:21:57'!
initializeWith: anObject stackedOver: aStacker

	stackedObject _ anObject.
	stackedOver _ aStacker! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MiddleStacker class' category: 'Stack-Exercise'!
MiddleStacker class
	instanceVariableNames: ''!

!MiddleStacker class methodsFor: 'as yet unclassified' stamp: 'TS 9/18/2021 22:35:56'!
with: anObject stackedOver: aStacker

	^self new initializeWith: anObject stackedOver: aStacker! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'size stackPointer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'TS 9/18/2021 22:21:10'!
initialize

	size _ 0.
	stackPointer _ BaseStacker new! !


!OOStack methodsFor: 'operations' stamp: 'TS 9/18/2021 22:27:41'!
isEmpty

	^size = 0
	"^stackPointer isKinfOf: BaseStacker"! !

!OOStack methodsFor: 'operations' stamp: 'TS 9/18/2021 22:31:10'!
pop
	
	|poppedObject|
	
	poppedObject _ stackPointer stackedObject.
	stackPointer _ stackPointer stackedOver.
	size _ size - 1.
	^poppedObject! !

!OOStack methodsFor: 'operations' stamp: 'TS 9/18/2021 22:36:13'!
push: anObject

	|aStacker|
	
	aStacker _ MiddleStacker with: anObject stackedOver: stackPointer.
	stackPointer _ aStacker.
	size _ size + 1.! !

!OOStack methodsFor: 'operations' stamp: 'TS 9/18/2021 22:19:44'!
size

	^size! !

!OOStack methodsFor: 'operations' stamp: 'TS 9/18/2021 22:32:17'!
top

	^stackPointer stackedObject ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'sentenceStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'parameter-validation' stamp: 'AR 9/20/2021 14:54:38'!
validatePrefix: aPrefix

	(aPrefix isKindOf: String) ifFalse: [self error: SentenceFinderByPrefix errorPrefixMustBeAString].
	(aPrefix size = 0) ifTrue: [self error: SentenceFinderByPrefix errorPrefixCantBeEmpty].
	(aPrefix includes: ' ') ifTrue: [self error: SentenceFinderByPrefix errorPrefixCantContainWhitespaces].! !

!SentenceFinderByPrefix methodsFor: 'parameter-validation' stamp: 'AR 9/20/2021 14:54:48'!
validateSentenceStack: aSentenceStack

	| aTemporalStack |

	(aSentenceStack isKindOf: OOStack) ifFalse: [self error: SentenceFinderByPrefix errorASentenceFinderMustBeInitializedWithAStack].
	
	aTemporalStack _ OOStack new.
	
	[aSentenceStack isEmpty not] whileTrue: [
		(aSentenceStack top isKindOf: String) ifFalse: [self error: SentenceFinderByPrefix errorStackMustOnlyContainStrings].
		aTemporalStack push: aSentenceStack pop].
	
	self reconstruct: aSentenceStack with: aTemporalStack.! !


!SentenceFinderByPrefix methodsFor: 'auxiliary' stamp: 'TS 9/18/2021 23:24:03'!
reconstruct: aStack with: aTemporalStack.

	[aTemporalStack isEmpty not] whileTrue: [aStack push: aTemporalStack pop]! !


!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'AR 9/20/2021 14:43:09'!
initializeWith: aSentenceStack

	self validateSentenceStack: aSentenceStack.

	sentenceStack _ aSentenceStack .! !


!SentenceFinderByPrefix methodsFor: 'finding' stamp: 'AR 9/20/2021 14:41:16'!
find: aPrefix

	|sentencesThatHaveThePrefix aTemporalStack|
	
	self validatePrefix: aPrefix.
	
	sentencesThatHaveThePrefix _ OrderedCollection new.
	aTemporalStack _ OOStack new.
	
	[sentenceStack isEmpty not] whileTrue: [
		(sentenceStack top beginsWith: aPrefix) ifTrue: [sentencesThatHaveThePrefix add: sentenceStack top]. 
		aTemporalStack push: sentenceStack pop.
		].
	
	self reconstruct: sentenceStack with: aTemporalStack.
	
	^sentencesThatHaveThePrefix! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error-descriptions' stamp: 'AR 9/20/2021 14:28:01'!
errorASentenceFinderMustBeInitializedWithAStack

	^ 'Sentence Finder requires an OOStack to be initialized'.! !

!SentenceFinderByPrefix class methodsFor: 'error-descriptions' stamp: 'AR 9/20/2021 14:38:30'!
errorPrefixCantBeEmpty

	^ 'Prefix must contain at least 1 character'.! !

!SentenceFinderByPrefix class methodsFor: 'error-descriptions' stamp: 'AR 9/20/2021 14:39:02'!
errorPrefixCantContainWhitespaces

	^ 'Prefix cannot contain any amount of whitespace'.! !

!SentenceFinderByPrefix class methodsFor: 'error-descriptions' stamp: 'AR 9/20/2021 14:33:56'!
errorPrefixMustBeAString

	^ 'The prefix parameter must be a String'.! !

!SentenceFinderByPrefix class methodsFor: 'error-descriptions' stamp: 'AR 9/20/2021 14:50:18'!
errorStackMustOnlyContainStrings

	^ 'Stack used to create a Finder must only contain Strings'.! !


!SentenceFinderByPrefix class methodsFor: 'creation' stamp: 'AR 9/20/2021 15:01:52'!
on: aSentenceStack

	^ self new initializeWith: aSentenceStack.! !
