!classDefinition: #EquipoTest category: 'ISW1-2021-2C-2doParcial'!
TestCase subclass: #EquipoTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:51:04'!
test01PrecioDebeSerUnaEntradaValida

	self
		should:[ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1 yCapacidad: 25 * meter * meter / day  ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorPrecioInvalido equals: unError messageText ]! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:50:59'!
test02CapacidadDebeSerUnaEntradaValida

	self
		should:[ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: 25 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorCapacidadInvalida equals: unError messageText ]! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:50:53'!
test03CapacidadDebeSerUnaEntradaValidaYPositiva

	self
		should:[ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: - 25 * meter * meter / day ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorCapacidadInvalida equals: unError messageText ]! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:49:40'!
test04PuedoPreguntarPrecioPorDiaConPrecioFijoYCapacidadFija

	| equipo |
	equipo _ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: 25 * meter * meter / day.

	self assert: (equipo precioPorDia) equals: 1000 * peso / day. ! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:49:46'!
test05PuedoPreguntarCapacidadPorDiaConPrecioFijoYCapacidadFija

	| equipo |
	equipo _ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: 25 * meter * meter / day.

	self assert: (equipo capacidadPorDia) equals: 25 * meter * meter / day. ! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:22:44'!
test06NoPuedoCrearEquipoConPrecioVariableSiAlgunPrecioEsInvalido

	| precios |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1500.
	
	self
		should: [EquipoPrecioVariableCapacidadFija crearEquipoConPrecios: precios cambiaPrecioALosDias: 5*day  yCapacidad: 25 * meter * meter / day.]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorPrecioInvalido equals: unError messageText ]

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:20:00'!
test07NoPuedoCrearEquipoConPrecioVariableSiDiaDeCambioEsInvalido

	| precios |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1500 * peso / day.
	
	self
		should: [EquipoPrecioVariableCapacidadFija crearEquipoConPrecios: precios cambiaPrecioALosDias: 5 yCapacidad: 25 * meter * meter / day.]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorDiaDeCambioInvalido equals: unError messageText ]

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:25:19'!
test08PuedoPreguntarPrecioEquipoConPrecioVariableCapacidadFija

	| precios equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1500 * peso / day.
	
	equipo _ EquipoPrecioVariableCapacidadFija crearEquipoConPrecios: precios cambiaPrecioALosDias: 5 * day yCapacidad: 25 * meter * meter / day.
	
	self assert: equipo preciosPorDia equals: precios

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:28:00'!
test09PuedoPreguntarCapacidadEquipoConPrecioVariableCapacidadFija

	| precios equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1500 * peso / day.
	
	equipo _ EquipoPrecioVariableCapacidadFija crearEquipoConPrecios: precios cambiaPrecioALosDias: 5 * day yCapacidad: 25 * meter * meter / day.
	
	self assert: equipo capacidadPorDia equals: 25 * meter * meter / day.

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:29:09'!
test10PuedoPreguntarDiaCambioDePrecioEquipoConPrecioVariableCapacidadFija

	| precios equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1500 * peso / day.
	
	equipo _ EquipoPrecioVariableCapacidadFija crearEquipoConPrecios: precios cambiaPrecioALosDias: 5 * day yCapacidad: 25 * meter * meter / day.
	
	self assert: equipo cambiaPrecioTrasDia equals: 5 * day.

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:59:29'!
test11NoPuedoCrearEquipoConCapacidadVariableSiAlgoEsInvalido

	| precio capacidades |
	
	precio _ 1500 * peso / day.
	capacidades _ OrderedCollection with: 150 * meter * meter / day with: 20.
	
	self
		should: [EquipoPrecioFijoCapacidadVariable crearEquipoConPrecio: precio yCapacidad: capacidades cambiaCapacidadALosDias: 5*day.]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorCapacidadInvalida equals: unError messageText ]

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:02:02'!
test12PuedoPedirPrecioAEquipoConPrecioFijoYCapacidadesVariables

	| precio capacidades equipo |
	
	precio _ 1500 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioFijoCapacidadVariable crearEquipoConPrecio: precio yCapacidad: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo precioPorDia equals: precio

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:04:56'!
test13PuedoPedirCapacidadesAEquipoConPrecioFijoYCapacidadesVariables

	| precio capacidades equipo |
	
	precio _ 1500 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioFijoCapacidadVariable crearEquipoConPrecio: precio yCapacidad: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo capacidadesPorDia equals: capacidades

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:05:58'!
test14PuedoPedirDiaDeCambioDeCapacidadAEquipoConPrecioFijoYCapacidadesVariables

	| precio capacidades equipo |
	
	precio _ 1500 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioFijoCapacidadVariable crearEquipoConPrecio: precio yCapacidad: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo cambiaCapacidadTras equals: 5*day

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:13:45'!
test15NoPuedoCrearEquipoConPrecioYCapacidadVariableSiAlgoEsInvalido

	| precios capacidades |
	
	precios _ OrderedCollection with: 1500 with: 1700 * peso / day.
	capacidades _ OrderedCollection with: 150 / day with: 20.
	
	self
		should: [EquipoPrecioVariableCapacidadVariable crearEquipoConPrecios: precios cambiaPrecioALosDias: 4 * day yCapacidades: capacidades cambiaCapacidadALosDias: 5*day.]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :unError | self assert: Equipo errorPrecioInvalido equals: unError messageText ]

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:27:40'!
test16PuedoPedirPrecioAEquipoConPrecioVariableYCapacidadesVariables

	| precios capacidades equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1700 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioVariableCapacidadVariable crearEquipoConPrecios: precios cambiaPrecioALosDias: 4 * day yCapacidades: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo preciosPorDia equals: precios

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:28:57'!
test17PuedoPedirCapacidadesAEquipoConPrecioVariableYCapacidadesVariables

	| precios capacidades equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1700 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioVariableCapacidadVariable crearEquipoConPrecios: precios cambiaPrecioALosDias: 4 * day yCapacidades: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo capacidadesPorDia equals: capacidades 

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:31:01'!
test18PuedoPedirDiaDeCambioDePrecioAEquipoConPrecioVariableYCapacidadesVariables

	| precios capacidades equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1700 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioVariableCapacidadVariable crearEquipoConPrecios: precios cambiaPrecioALosDias: 4 * day yCapacidades: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo cambiaPrecioTras equals: 4*day

	! !

!EquipoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:31:57'!
test19PuedoPedirDiaDeCambioDeCapacidadAEquipoConPrecioVariableYCapacidadesVariables

	| precios capacidades equipo |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1700 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioVariableCapacidadVariable crearEquipoConPrecios: precios cambiaPrecioALosDias: 4 * day yCapacidades: capacidades cambiaCapacidadALosDias: 5*day.
	
	self assert: equipo cambiaCapacidadTras equals: 5*day

	! !


!classDefinition: #PresupuestoTest category: 'ISW1-2021-2C-2doParcial'!
TestCase subclass: #PresupuestoTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!PresupuestoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:17:16'!
test01PuedoCalcularTotalCorrectamenteAEquipoConPrecioFijoYCapacidadeFija

	| equipo presupuesto |
	equipo _ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: 25 * meter * meter / day.
	
	presupuesto _ Presupuesto conGrupo: (OrderedCollection with: equipo).

	self assert: (presupuesto precioTotalPor: 10*day ) equals:  250000 * peso.
! !

!PresupuestoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:24:52'!
test02NoPuedoCalcularTotalCorrectamenteAEquipoConPrecioFijoYCapacidadeFijaSiLosDiasDeConstruccionNoSonDias

	| equipo presupuesto |
	equipo _ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: 25 * meter * meter / day.
	
	presupuesto _ Presupuesto conGrupo: (OrderedCollection with: equipo).

	self 
		should:[ presupuesto precioTotalPor: 10 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [:error | self assert: Presupuesto errorDiasDeConstruccionInvalidos equals: error messageText ]
	
	
! !

!PresupuestoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:27:43'!
test03NoPuedoCalcularTotalCorrectamenteAEquipoConPrecioFijoYCapacidadeFijaSiLosDiasDeConstruccionSonNegativos

	| equipo presupuesto |
	equipo _ EquipoPrecioFijoCapacidadFija crearEquipoConPrecio: 1000 * peso / day yCapacidad: 25 * meter * meter / day.
	
	presupuesto _ Presupuesto conGrupo: (OrderedCollection with: equipo).

	self 
		should:[ presupuesto precioTotalPor: - 10 * day ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [:error | self assert: Presupuesto errorDiasDeConstruccionInvalidos equals: error messageText ]
	
	
! !

!PresupuestoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:21:22'!
test04PuedoCalcularCorrectamentePresupuestoConEquipoConPrecioVariableCapacidadFija

	| precios equipo presupuesto |
	
	precios _ OrderedCollection with: 1000 * peso / day with: 1500 * peso / day.
	
	equipo _ EquipoPrecioVariableCapacidadFija crearEquipoConPrecios: precios cambiaPrecioALosDias: 5 * day yCapacidad: 25 * meter * meter / day.
	
	presupuesto _ Presupuesto conGrupo: (OrderedCollection with: equipo).
	
	self assert: (presupuesto precioTotalPor: 10*day ) equals:  312500 * peso .

	! !

!PresupuestoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:36:52'!
test05PuedoCalcularCorrectamentePresupuestoConEquipoConPrecioFijoCapacidadVariable

	| precio capacidades equipo presupuesto |
	
	precio _ 1500 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioFijoCapacidadVariable crearEquipoConPrecio: precio yCapacidad: capacidades cambiaCapacidadALosDias: 5*day.
	
	presupuesto _ Presupuesto conGrupo: (OrderedCollection with: equipo).
	
	self assert: (presupuesto precioTotalPor: 10*day ) equals:  1275000 * peso .

	! !

!PresupuestoTest methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:50:10'!
test06PuedoCalcularCorrectamentePresupuestoConEquipoConPrecioVariableCapacidadVariable

	| precios capacidades equipo presupuesto |
	
	precios _ OrderedCollection with: 1500 * peso / day with: 1700 * peso / day.
	capacidades _ OrderedCollection with: (150 * meter * meter / day) with: (20 * meter * meter / day ).
	
	equipo _ EquipoPrecioVariableCapacidadVariable crearEquipoConPrecios: precios cambiaPrecioALosDias: 4 * day yCapacidades: capacidades cambiaCapacidadALosDias: 5*day.
	
	presupuesto _ Presupuesto conGrupo: (OrderedCollection with: equipo).
	
	"self assert: (presupuesto precioTotalPor: 10*day ) equals:  1275000 * peso .
	no llegu� a terminarlo
	"

	! !


!classDefinition: #Equipo category: 'ISW1-2021-2C-2doParcial'!
Object subclass: #Equipo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!


!Equipo methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:23:20'!
assertEsUnDiaValido: dia

	^ (dia isKindOf: Measure) ifFalse: [self error: Equipo errorDiaDeCambioInvalido]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Equipo class' category: 'ISW1-2021-2C-2doParcial'!
Equipo class
	instanceVariableNames: ''!


!Equipo class methodsFor: 'error' stamp: 'august 11/25/2021 18:50:37'!
errorCapacidadInvalida
	
	^ 'Capacidad Invalida'! !

!Equipo class methodsFor: 'error' stamp: 'august 11/25/2021 19:21:47'!
errorDiaDeCambioInvalido
	
	^ 'Dia de cambio invalido'! !

!Equipo class methodsFor: 'error' stamp: 'august 11/25/2021 18:50:28'!
errorPrecioInvalido
	
	^ 'Precio invalido'! !


!classDefinition: #EquipoPrecioFijoCapacidadFija category: 'ISW1-2021-2C-2doParcial'!
Equipo subclass: #EquipoPrecioFijoCapacidadFija
	instanceVariableNames: 'precioPorDia capacidadPorDia'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!EquipoPrecioFijoCapacidadFija methodsFor: 'initialization' stamp: 'august 11/25/2021 18:47:20'!
assertSonValidosElPrecio: unPrecioPorDia yLaCapacidad: unaCapacidadPorDia

	(unPrecioPorDia isKindOf: Measure) ifFalse: [ self error: Equipo errorPrecioInvalido].

	(unaCapacidadPorDia isKindOf: Measure) ifFalse: [self error: Equipo errorCapacidadInvalida].
	
	(unaCapacidadPorDia > 0) ifFalse: [self error: Equipo errorCapacidadInvalida]! !

!EquipoPrecioFijoCapacidadFija methodsFor: 'initialization' stamp: 'august 11/25/2021 18:47:41'!
initializeCrearEquipoConPrecio: unPrecioPorDia yCapacidad: unaCapacidadPorDia 
	
	self assertSonValidosElPrecio: unPrecioPorDia yLaCapacidad: unaCapacidadPorDia.
		
	precioPorDia := unPrecioPorDia.
	capacidadPorDia := unaCapacidadPorDia.! !


!EquipoPrecioFijoCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:47:35'!
capacidadPorDia
	
	^capacidadPorDia! !

!EquipoPrecioFijoCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:51:02'!
precioDeConstruccionPor: unosDias 
	
	^ unosDias * capacidadPorDia  * precioPorDia  * (day / (meter * meter))
	! !

!EquipoPrecioFijoCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 18:47:53'!
precioPorDia
	
	^ precioPorDia ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EquipoPrecioFijoCapacidadFija class' category: 'ISW1-2021-2C-2doParcial'!
EquipoPrecioFijoCapacidadFija class
	instanceVariableNames: ''!

!EquipoPrecioFijoCapacidadFija class methodsFor: 'instance creation' stamp: 'august 11/25/2021 18:46:30'!
crearEquipoConPrecio: unPrecioPorDia yCapacidad: unaCapacidadPorDia
	
	^self new initializeCrearEquipoConPrecio: unPrecioPorDia yCapacidad: unaCapacidadPorDia ! !



!classDefinition: #EquipoPrecioFijoCapacidadVariable category: 'ISW1-2021-2C-2doParcial'!
Equipo subclass: #EquipoPrecioFijoCapacidadVariable
	instanceVariableNames: 'preciosPorDia capacidadesPorDia cambiaCapacidadTras'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!EquipoPrecioFijoCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:03:06'!
assertSonValidosLosPrecios: unPrecioPorDia yUnasCapacidades: unasCapacidadesPorDia

	(unPrecioPorDia isKindOf: Measure) ifFalse: [ self error: Equipo errorPrecioInvalido].

	(unasCapacidadesPorDia allSatisfy: [:unaCapacidadPorDia | unaCapacidadPorDia isKindOf: Measure]) ifFalse: [self error: Equipo errorCapacidadInvalida].
	
	(unasCapacidadesPorDia allSatisfy: [:unaCapacidadPorDia | unaCapacidadPorDia > 0 ] ) ifFalse: [self error: Equipo errorCapacidadInvalida]
	
	! !

!EquipoPrecioFijoCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:06:14'!
cambiaCapacidadTras
	
	^cambiaCapacidadTras! !

!EquipoPrecioFijoCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:04:23'!
capacidadesPorDia
	
	^capacidadesPorDia! !

!EquipoPrecioFijoCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:34:02'!
precioDeConstruccionPor: totalDias 
	
	| primerosDias |
	primerosDias _ totalDias - cambiaCapacidadTras.
	
	^ primerosDias * preciosPorDia * capacidadesPorDia first + ( cambiaCapacidadTras * preciosPorDia * capacidadesPorDia last ) * (day / (meter * meter))
	! !

!EquipoPrecioFijoCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:03:41'!
precioPorDia
	
	^ preciosPorDia.! !


!EquipoPrecioFijoCapacidadVariable methodsFor: 'initialization' stamp: 'august 11/25/2021 20:23:56'!
initializecrearEquipoConPrecio: unPrecio yCapacidad: unasCapacidades cambiaCapacidadALosDias: unDia 
	
	self assertSonValidosLosPrecios: unPrecio yUnasCapacidades: unasCapacidades.
	
	preciosPorDia _ unPrecio .
	capacidadesPorDia _ unasCapacidades .
	
	self assertEsUnDiaValido: unDia.
	cambiaCapacidadTras _ unDia.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EquipoPrecioFijoCapacidadVariable class' category: 'ISW1-2021-2C-2doParcial'!
EquipoPrecioFijoCapacidadVariable class
	instanceVariableNames: ''!

!EquipoPrecioFijoCapacidadVariable class methodsFor: 'instance creation' stamp: 'august 11/25/2021 19:51:07'!
crearEquipoConPrecio: unPrecio yCapacidad: unasCapacidades cambiaCapacidadALosDias: unDia 
	
	^self new initializecrearEquipoConPrecio: unPrecio yCapacidad: unasCapacidades cambiaCapacidadALosDias: unDia ! !


!classDefinition: #EquipoPrecioVariableCapacidadFija category: 'ISW1-2021-2C-2doParcial'!
Equipo subclass: #EquipoPrecioVariableCapacidadFija
	instanceVariableNames: 'preciosPorDia capacidadesPorDia cambiaPrecioTras'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!EquipoPrecioVariableCapacidadFija methodsFor: 'initialization' stamp: 'august 11/25/2021 19:31:44'!
initializeCrearEquipoConPrecios: unosPrecios cambiaPrecioALosDias: dia yCapacidad: unaCapacidad 
	
	self assertSonValidosLosPrecios: unosPrecios yLaCapacidad: unaCapacidad.
	
	preciosPorDia := unosPrecios.
	capacidadesPorDia := unaCapacidad.
	
	self assertEsUnDiaValido: dia.
	cambiaPrecioTras:= dia.! !


!EquipoPrecioVariableCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:09:16'!
assertSonValidosLosPrecios: unosPreciosPorDia yLaCapacidad: unaCapacidadPorDia

	(unosPreciosPorDia allSatisfy: [:unPrecioPorDia | unPrecioPorDia isKindOf: Measure]) ifFalse: [ self error: Equipo errorPrecioInvalido].

	(unaCapacidadPorDia isKindOf: Measure) ifFalse: [self error: Equipo errorCapacidadInvalida].
	
	(unaCapacidadPorDia > 0) ifFalse: [self error: Equipo errorCapacidadInvalida]! !

!EquipoPrecioVariableCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:29:52'!
cambiaPrecioTrasDia
	
	^ cambiaPrecioTras .! !

!EquipoPrecioVariableCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:27:07'!
capacidadPorDia
	
	^ capacidadesPorDia copy.! !

!EquipoPrecioVariableCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:01:34'!
precioDeConstruccionPor: totalDias 
	
	| primerosDias |
	primerosDias _ totalDias - cambiaPrecioTras.
	
	^ primerosDias * preciosPorDia first * capacidadesPorDia + ( cambiaPrecioTras * preciosPorDia last * capacidadesPorDia ) * (day / (meter * meter))
	
	! !

!EquipoPrecioVariableCapacidadFija methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 19:25:43'!
preciosPorDia
	
	^preciosPorDia! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EquipoPrecioVariableCapacidadFija class' category: 'ISW1-2021-2C-2doParcial'!
EquipoPrecioVariableCapacidadFija class
	instanceVariableNames: ''!

!EquipoPrecioVariableCapacidadFija class methodsFor: 'instance creation' stamp: 'august 11/25/2021 19:15:55'!
crearEquipoConPrecios: unosPrecios cambiaPrecioALosDias: dia yCapacidad: unaCapacidad
	
	^self new initializeCrearEquipoConPrecios: unosPrecios cambiaPrecioALosDias: dia yCapacidad: unaCapacidad. ! !


!classDefinition: #EquipoPrecioVariableCapacidadVariable category: 'ISW1-2021-2C-2doParcial'!
Equipo subclass: #EquipoPrecioVariableCapacidadVariable
	instanceVariableNames: 'preciosPorDia diaDeCambioDePrecio capacidadesPorDia diaDeCambioDeCapacidades'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!EquipoPrecioVariableCapacidadVariable methodsFor: 'initialization' stamp: 'august 11/25/2021 20:25:15'!
initializeCrearEquipoConPrecios: unosPrecios cambiaPrecioALosDias: unDiaDeCambioDePrecio yCapacidades: unasCapacidades cambiaCapacidadALosDias: unDiaDeCambioDeCapacidades 
	
	
	self assertSonValidosLosPrecios: unosPrecios yLasCapacidades: unasCapacidades.
	
	preciosPorDia := unosPrecios.
	capacidadesPorDia := unasCapacidades.
	
	self assertEsUnDiaValido: unDiaDeCambioDePrecio.
	diaDeCambioDePrecio := unDiaDeCambioDePrecio.
	
	self assertEsUnDiaValido: unDiaDeCambioDeCapacidades.
	diaDeCambioDeCapacidades := unDiaDeCambioDeCapacidades.! !


!EquipoPrecioVariableCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:21:42'!
assertSonValidosLosPrecios: unosPreciosPorDia yLasCapacidades: unasCapacidadesPorDia

	(unosPreciosPorDia allSatisfy: [:unPrecioPorDia | unPrecioPorDia isKindOf: Measure]) ifFalse: [ self error: Equipo errorPrecioInvalido].

	(unasCapacidadesPorDia allSatisfy: [:unaCapacidadPorDia | (unaCapacidadPorDia isKindOf: Measure) and:[unaCapacidadPorDia > 0] ]) ifFalse: [ self error: Equipo errorCapacidadInvalida].
	! !

!EquipoPrecioVariableCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:32:32'!
cambiaCapacidadTras
	
	^ diaDeCambioDeCapacidades ! !

!EquipoPrecioVariableCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:31:26'!
cambiaPrecioTras
	
	^ diaDeCambioDePrecio ! !

!EquipoPrecioVariableCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:28:48'!
capacidadesPorDia
	
	^capacidadesPorDia! !

!EquipoPrecioVariableCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:49:48'!
precioDeConstruccionPor: totalDias 
	
	"est� mal. no llegu� a terminarlo"
	
	| diasSinCambio |
	diasSinCambio _  (totalDias - (diaDeCambioDePrecio + diaDeCambioDeCapacidades)) abs.
	
	^ diasSinCambio * preciosPorDia first * capacidadesPorDia + ( 1 * preciosPorDia last * capacidadesPorDia ) * (day / (meter * meter))
	
	! !

!EquipoPrecioVariableCapacidadVariable methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 20:27:58'!
preciosPorDia
	
	^preciosPorDia! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EquipoPrecioVariableCapacidadVariable class' category: 'ISW1-2021-2C-2doParcial'!
EquipoPrecioVariableCapacidadVariable class
	instanceVariableNames: ''!

!EquipoPrecioVariableCapacidadVariable class methodsFor: 'instance creation' stamp: 'august 11/25/2021 20:16:17'!
crearEquipoConPrecios: unosPrecios cambiaPrecioALosDias: unDiaDeCambioDePrecio yCapacidades: unasCapacidades cambiaCapacidadALosDias: unDiaDeCambioDeCapacidades 
	
	^self new initializeCrearEquipoConPrecios: unosPrecios cambiaPrecioALosDias: unDiaDeCambioDePrecio yCapacidades: unasCapacidades cambiaCapacidadALosDias: unDiaDeCambioDeCapacidades ! !


!classDefinition: #Presupuesto category: 'ISW1-2021-2C-2doParcial'!
Object subclass: #Presupuesto
	instanceVariableNames: 'grupoEquipos'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-2doParcial'!

!Presupuesto methodsFor: 'initialization' stamp: 'august 11/25/2021 21:18:37'!
initializeConEquipos: unosEquipos 
	
	grupoEquipos := unosEquipos.! !


!Presupuesto methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:50:44'!
assertSonDiasValidos: diasDeConstruccion

	^ ((diasDeConstruccion isKindOf: Measure) and: [ diasDeConstruccion > 0]) ifFalse: [ self error: Presupuesto errorDiasDeConstruccionInvalidos]! !

!Presupuesto methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:51:10'!
calcularTotal: diasDeConstruccion

	^ grupoEquipos first precioDeConstruccionPor: diasDeConstruccion! !

!Presupuesto methodsFor: 'as yet unclassified' stamp: 'august 11/25/2021 21:51:16'!
precioTotalPor: diasDeConstruccion

	self assertSonDiasValidos: diasDeConstruccion.
	
	^self calcularTotal: diasDeConstruccion .
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Presupuesto class' category: 'ISW1-2021-2C-2doParcial'!
Presupuesto class
	instanceVariableNames: ''!

!Presupuesto class methodsFor: 'instance creation' stamp: 'august 11/25/2021 21:17:01'!
conGrupo: equipos

	^self new initializeConEquipos: equipos! !


!Presupuesto class methodsFor: 'error' stamp: 'august 11/25/2021 21:27:11'!
errorDiasDeConstruccionInvalidos
	
	^ 'El periodo de d�as de construccion es invalido'! !
