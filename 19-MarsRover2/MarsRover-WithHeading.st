!classDefinition: #MarsRoverObserverTests category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverObserverTests
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:06:03'!
test01MarsRoverLoggerHasAnEmptyLogWhenNoActivity

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	
	self verify: logger 
	isComposedOf: OrderedCollection new 
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:07:01'!
test02MarsRoverLoggerLogsForwardMovement
	
	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: '1@2')
	after: [logger requestObservationOf: marsRover.  logger trackPositionChanges. marsRover process: 'f'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:07:36'!
test03MarsRoverLoggerLogsBackwardsMovement
	
	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: '1@0')
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges. marsRover process: 'b'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:09:06'!
test04MarsRoverLoggerTrackingPositionIgnoresHeadingChanges

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: OrderedCollection new
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges. marsRover process: 'l'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:10:21'!
test05MarsRoverLoggerTrackingPositionCanLogMultiplePositionChanges

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: '1@2' with: '2@2' with: '2@1' with: '1@1')
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges. marsRover process: 'frfrfrf'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:11:50'!
test06MarsRoverLoggerTrackingHeadingLogsRightRotation

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: #East)
	after: [logger requestObservationOf: marsRover. logger trackHeadingChanges. marsRover process: 'r'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:12:10'!
test07MarsRoverLoggerTrackingHeadingLogsMultipleRightRotations

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: #East with: #South with: #West with: #North)
	after: [logger requestObservationOf: marsRover. logger trackHeadingChanges. marsRover process: 'rrrr'.] 
	on: marsRover.

	
	! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:12:27'!
test08MarsRoverLoggerTrackingHeadingLogsLeftRotation

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: #West)
	after: [logger requestObservationOf: marsRover. logger trackHeadingChanges. marsRover process: 'l'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:12:58'!
test09MarsRoverLoggerTrackingHeadingLogsMultipleLeftRotations

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: #West with: #South with: #East with: #North)
	after: [logger requestObservationOf: marsRover. logger trackHeadingChanges. marsRover process: 'llll'.] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:13:20'!
test10MarsRoverLoggerTrackingHeadingIgnoresMovementChanges

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: OrderedCollection new
	after: [logger requestObservationOf: marsRover. logger trackHeadingChanges. marsRover process: 'fb'.] 
	on: marsRover.
	
	


	
	! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:13:43'!
test11MarsRoverLoggerTrackingPositionIgnoresHeadingChages

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: OrderedCollection new
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges. marsRover process: 'lr'.] 
	on: marsRover.
	
	


	
	! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:14:04'!
test12MarsRoverLoggerCanTrackBothPositionAndHeading

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: '1@2' with: #West with: #North with: '1@1')
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges. logger trackHeadingChanges. marsRover process: 'flrb'.] 
	on: marsRover.
	
	


	
	! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:14:25'!
test13MarsRoverLoggerIgnoresChangesAfterUntrackingPosition

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: '1@2')
	after: [logger requestObservationOf: marsRover. logger trackPositionChanges . marsRover process: 'f'. logger untrackPositionChanges. marsRover process: 'f'] 
	on: marsRover.
	
	


	
	! !

!MarsRoverObserverTests methodsFor: 'logger' stamp: 'AR 10/28/2021 14:14:42'!
test14MarsRoverLoggerIgnoresChangesAfterUNtrackingHeading

	|  logger marsRover |
	
	logger := MarsRoverLogger new.	 
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	self verify: logger 
	isComposedOf: (OrderedCollection with: #East)
	after: [logger requestObservationOf: marsRover. logger trackHeadingChanges . marsRover process: 'r'. logger untrackHeadingChanges. marsRover process: 'r'] 
	on: marsRover.
	
	


	
	! !


!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:15:41'!
test15MarsRoverDisplayStartsWithNoTextFields

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: '' 
	after: [display requestObservationOf: marsRover. display trackPositionChanges ] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:16:31'!
test16MarsRoverDisplayCanDisplayForwardMovement

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: '1@2'
	hasHeadingTextField: '' 
	after: [display requestObservationOf: marsRover. display trackPositionChanges. marsRover process: 'f'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:16:57'!
test17MarsRoverDisplayCanDisplayBackwardsMovement

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: '1@0'
	hasHeadingTextField: '' 
	after: [display requestObservationOf: marsRover. display trackPositionChanges. marsRover process: 'b'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:17:16'!
test18MarsRoverDisplayTrackingPositionIgnoresHeadingChanges

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: '' 
	after: [display requestObservationOf: marsRover. display trackPositionChanges. marsRover process: 'l'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:17:51'!
test19MarsRoverDisplayTrackingPositionDisplaysLastPosition

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: '1@1'
	hasHeadingTextField: '' 
	after: [display requestObservationOf: marsRover. display trackPositionChanges. marsRover process: 'frfrfrf'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:18:45'!
test20MarsRoverDisplayCanTrackRightRotation

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: #East
	after: [display requestObservationOf: marsRover. display trackHeadingChanges. marsRover process: 'r'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:19:00'!
test21MarsRoverDisplayCanTrackMultipleRightRotations

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: #West
	after: [display requestObservationOf: marsRover. display trackHeadingChanges. marsRover process: 'rrr'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:20:23'!
test22MarsRoverDisplayCanTrackLeftRotation

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: #West
	after: [display requestObservationOf: marsRover. display trackHeadingChanges. marsRover process: 'l'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:20:54'!
test23MarsRoverDisplayCanTrackMultipleLeftRotations

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: #East
	after: [display requestObservationOf: marsRover. display trackHeadingChanges. marsRover process: 'lll'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:21:32'!
test24MarsRoverDisplayTrackingBothHeadingAndPositionDisplaysTheirLastPositionAndHeading

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: '2@2'
	hasHeadingTextField: #South
	after: [display requestObservationOf: marsRover. display trackHeadingChanges. display trackPositionChanges. marsRover process: 'frfr'] 
	on: marsRover.! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:21:58'!
test25MarsRoverDisplayCanStopTrackingHeadingChanges

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: ''
	hasHeadingTextField: #East
	after: [display requestObservationOf: marsRover. display trackHeadingChanges. marsRover process: 'r'. display untrackHeadingChanges . marsRover process: 'r'.] 
	on: marsRover.
! !

!MarsRoverObserverTests methodsFor: 'display' stamp: 'AR 10/28/2021 14:22:25'!
test26MarsRoverDisplayCanStopTrackingPositionChanges

	| marsRover display |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth .
	display := MarsRoverDisplay new.
	
	self verify: display
	hasPositionTextField: '1@2'
	hasHeadingTextField: ''
	after: [display requestObservationOf: marsRover. display trackPositionChanges. marsRover process: 'f'. display untrackPositionChanges . marsRover process: 'f'.] 
	on: marsRover.
! !


!MarsRoverObserverTests methodsFor: 'verification' stamp: 'AR 10/28/2021 13:56:22'!
verify: aDisplayObserver hasPositionTextField: aPosition hasHeadingTextField: aHeading after: aClosure on: aMarsRover
	aClosure value.
		
	self assert: aPosition equals: aDisplayObserver positionTextFieldModel .
	self assert: aHeading equals: aDisplayObserver headingTextFieldModel .
	! !

!MarsRoverObserverTests methodsFor: 'verification' stamp: 'AR 10/28/2021 13:33:12'!
verify: aLoggerObserver isComposedOf: aStringCollection after: aClosure on: aMarsRover

	| log |

	aClosure value.
		
	log := ReadStream on: aLoggerObserver getLog contents. 
	
	aStringCollection do: [:aString | self assert: aString equals: log nextLine].
	self assert: log atEnd.
	! !


!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head observers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'acting on commands' stamp: 'ts 10/7/2021 01:08:12'!
moveBackwards

	position _ position - heading cartesianRepresentation.	! !


!MarsRover methodsFor: 'initialization' stamp: 'ts 10/28/2021 11:03:49'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	observers := OrderedCollection new.! !


!MarsRover methodsFor: 'processing commands' stamp: 'ts 10/7/2021 01:27:14'!
isValidCommand: aPotentialCommandCharacter

	^'fblr' includesSubString: (aPotentialCommandCharacter asString)! !


!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'heading' stamp: 'ts 10/28/2021 11:05:57'!
headEast
	
	head := MarsRoverHeadingEast for: self.
	observers do: [:aMarsRoverObserver |
		aMarsRoverObserver notifyChangingHeadingToEast.
		]! !

!MarsRover methodsFor: 'heading' stamp: 'ts 10/28/2021 11:06:10'!
headNorth
	
	head := MarsRoverHeadingNorth for: self.
	observers do: [:aMarsRoverObserver |
		aMarsRoverObserver notifyChangingHeadingToNorth.
		]! !

!MarsRover methodsFor: 'heading' stamp: 'ts 10/28/2021 11:06:22'!
headSouth
	
	head := MarsRoverHeadingSouth for: self.
	observers do: [:aMarsRoverObserver |
		aMarsRoverObserver notifyChangingHeadingToSouth.
		]! !

!MarsRover methodsFor: 'heading' stamp: 'ts 10/28/2021 11:06:34'!
headWest
	
	head := MarsRoverHeadingWest for: self.
	observers do: [:aMarsRoverObserver |
		aMarsRoverObserver notifyChangingHeadingToWest.
		]! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'AR 10/25/2021 21:24:02'!
rotateRight
	
	head rotateRight.! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 10:57:40'!
moveBackward
	
	head moveBackward.
! !

!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 11:04:50'!
moveBy: aPositionDelta
	
	position := position + aPositionDelta.
	observers do: [:aMarsRoverObserver |
		aMarsRoverObserver notifyMovingTo: position.
		]
	! !

!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 10:56:05'!
moveEast
	
	self moveBy: (1@0).! !

!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 10:57:32'!
moveForward
	
	head moveForward.
! !

!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 10:56:32'!
moveNorth
	
	self moveBy: (0@1).! !

!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 10:56:58'!
moveSouth
	
	self moveBy: (0@-1)! !

!MarsRover methodsFor: 'moving' stamp: 'ts 10/28/2021 10:57:23'!
moveWest
	
	self moveBy: (-1@0).! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'as yet unclassified' stamp: 'ts 10/28/2021 11:02:39'!
acceptObserver: aMarsRoverObserver

	observers add: aMarsRoverObserver! !

!MarsRover methodsFor: 'as yet unclassified' stamp: 'ts 10/28/2021 11:03:12'!
terminateObserver: aMarsRoverObserver

	observers remove: aMarsRoverObserver.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!classDefinition: #MarsRoverObserver category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverObserver
	instanceVariableNames: 'observee'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverObserver methodsFor: 'notifications' stamp: 'ts 10/28/2021 10:14:43'!
notifyChangingHeadingToEast

	^self subclassResponsibility ! !

!MarsRoverObserver methodsFor: 'notifications' stamp: 'ts 10/28/2021 10:15:08'!
notifyChangingHeadingToNorth

	^self subclassResponsibility ! !

!MarsRoverObserver methodsFor: 'notifications' stamp: 'ts 10/28/2021 10:15:15'!
notifyChangingHeadingToSouth

	^self subclassResponsibility ! !

!MarsRoverObserver methodsFor: 'notifications' stamp: 'ts 10/28/2021 10:14:59'!
notifyChangingHeadingToWest

	^self subclassResponsibility ! !

!MarsRoverObserver methodsFor: 'notifications' stamp: 'ts 10/28/2021 10:32:44'!
notifyMovingTo: aMarsRoverPosition

	^self subclassResponsibility ! !


!MarsRoverObserver methodsFor: 'observation managing' stamp: 'ts 10/28/2021 11:09:03'!
requestObservationOf: aPotentialObservee

	^self subclassResponsibility ! !

!MarsRoverObserver methodsFor: 'observation managing' stamp: 'ts 10/28/2021 11:09:11'!
terminateObservation

	^self subclassResponsibility ! !


!classDefinition: #HeadingManager category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #HeadingManager
	instanceVariableNames: 'observant'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadingManager methodsFor: 'initialization' stamp: 'ts 10/28/2021 14:51:15'!
initializeFor: anObserver

	observant := anObserver! !


!HeadingManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:53:09'!
notifyChangingHeadingToEast

	observant notifyChangingHeadingToEast! !

!HeadingManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:52:55'!
notifyChangingHeadingToNorth

	observant notifyChangingHeadingToNorth
! !

!HeadingManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:52:12'!
notifyChangingHeadingToSouth

	observant notifyChangingHeadingToSouth! !

!HeadingManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:51:52'!
notifyChangingHeadingToWest

	observant notifyChangingHeadingToWest! !

!HeadingManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:51:36'!
notifyMovingTo: aMarsRoverPosition! !


!HeadingManager methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:51:15'!
requestObservationOf: aPotentialObservee

	aPotentialObservee acceptObserver: self.
	observee := aPotentialObservee! !

!HeadingManager methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:51:15'!
terminateObservation

	observee terminateObserver: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeadingManager class' category: 'MarsRover-WithHeading'!
HeadingManager class
	instanceVariableNames: ''!

!HeadingManager class methodsFor: 'as yet unclassified' stamp: 'ts 10/28/2021 14:51:15'!
for: anObserver

	^self new initializeFor: anObserver ! !


!classDefinition: #MarsRoverDisplay category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #MarsRoverDisplay
	instanceVariableNames: 'positionTextField headingTextField positionTracker headingTracker'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverDisplay methodsFor: 'display' stamp: 'AR 10/28/2021 13:09:09'!
headingTextFieldModel
	
	^ headingTextField .! !

!MarsRoverDisplay methodsFor: 'display' stamp: 'AR 10/28/2021 12:59:57'!
positionTextFieldModel
	
	^ positionTextField .! !


!MarsRoverDisplay methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:59:22'!
notifyChangingHeadingToEast

	headingTextField := #East! !

!MarsRoverDisplay methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:59:33'!
notifyChangingHeadingToNorth

	headingTextField := #North! !

!MarsRoverDisplay methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:59:47'!
notifyChangingHeadingToSouth

	headingTextField := #South! !

!MarsRoverDisplay methodsFor: 'notifications' stamp: 'ts 10/28/2021 15:00:00'!
notifyChangingHeadingToWest

	headingTextField  := #West! !

!MarsRoverDisplay methodsFor: 'notifications' stamp: 'ts 10/28/2021 15:00:09'!
notifyMovingTo: aMarsRoverPosition

	positionTextField := aMarsRoverPosition asString! !


!MarsRoverDisplay methodsFor: 'observation managing' stamp: 'ts 10/28/2021 15:00:39'!
requestObservationOf: aPotentialObservee

	observee := aPotentialObservee.
	positionTextField := ''.
	headingTextField := ''.
! !

!MarsRoverDisplay methodsFor: 'observation managing' stamp: 'ts 10/28/2021 15:01:41'!
trackHeadingChanges

	headingTracker requestObservationOf: observee! !

!MarsRoverDisplay methodsFor: 'observation managing' stamp: 'ts 10/28/2021 15:03:49'!
trackPositionChanges

	positionTracker requestObservationOf: observee! !

!MarsRoverDisplay methodsFor: 'observation managing' stamp: 'ts 10/28/2021 15:01:55'!
untrackHeadingChanges

	headingTracker terminateObservation ! !

!MarsRoverDisplay methodsFor: 'observation managing' stamp: 'ts 10/28/2021 15:03:49'!
untrackPositionChanges

	positionTracker terminateObservation ! !


!MarsRoverDisplay methodsFor: 'initialization' stamp: 'ts 10/28/2021 15:03:49'!
initialize

	positionTracker := PositionManager for: self.
	headingTracker := HeadingManager for: self.! !


!classDefinition: #MarsRoverLogger category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #MarsRoverLogger
	instanceVariableNames: 'log postitionTracker headingTracker'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverLogger methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:55:49'!
notifyChangingHeadingToEast

	log nextPutAll: #East; newLine! !

!MarsRoverLogger methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:55:58'!
notifyChangingHeadingToNorth

	log nextPutAll: #North; newLine! !

!MarsRoverLogger methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:56:06'!
notifyChangingHeadingToSouth

	log nextPutAll: #South; newLine! !

!MarsRoverLogger methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:56:16'!
notifyChangingHeadingToWest

	log nextPutAll: #West; newLine! !

!MarsRoverLogger methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:42:42'!
notifyMovingTo: aMarsRoverPosition

	log nextPutAll: aMarsRoverPosition asString; newLine! !


!MarsRoverLogger methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:56:29'!
requestObservationOf: aPotentialObservee

	observee := aPotentialObservee.
	log := WriteStream on: ''.! !

!MarsRoverLogger methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:54:39'!
trackHeadingChanges

	headingTracker requestObservationOf: observee! !

!MarsRoverLogger methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:41:22'!
trackPositionChanges

	postitionTracker requestObservationOf: observee! !

!MarsRoverLogger methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:54:54'!
untrackHeadingChanges

	headingTracker terminateObservation ! !

!MarsRoverLogger methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:42:20'!
untrackPositionChanges

	postitionTracker terminateObservation ! !


!MarsRoverLogger methodsFor: 'initialization' stamp: 'ts 10/28/2021 14:54:17'!
initialize

	postitionTracker := PositionManager for: self.
	headingTracker := HeadingManager for: self.
	! !


!MarsRoverLogger methodsFor: 'log' stamp: 'ts 10/28/2021 10:44:43'!
getLog

	^log copy! !


!classDefinition: #PositionManager category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #PositionManager
	instanceVariableNames: 'observant'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!PositionManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:35:39'!
notifyChangingHeadingToEast! !

!PositionManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:35:49'!
notifyChangingHeadingToNorth
! !

!PositionManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:35:58'!
notifyChangingHeadingToSouth! !

!PositionManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:36:07'!
notifyChangingHeadingToWest! !

!PositionManager methodsFor: 'notifications' stamp: 'ts 10/28/2021 14:36:42'!
notifyMovingTo: aMarsRoverPosition

	observant notifyMovingTo: aMarsRoverPosition! !


!PositionManager methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:49:24'!
requestObservationOf: aPotentialObservee

	aPotentialObservee acceptObserver: self.
	observee := aPotentialObservee! !

!PositionManager methodsFor: 'observation managing' stamp: 'ts 10/28/2021 14:38:55'!
terminateObservation

	observee terminateObserver: self! !


!PositionManager methodsFor: 'initialization' stamp: 'ts 10/28/2021 14:44:12'!
initializeFor: anObserver

	observant := anObserver! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PositionManager class' category: 'MarsRover-WithHeading'!
PositionManager class
	instanceVariableNames: ''!

!PositionManager class methodsFor: 'as yet unclassified' stamp: 'ts 10/28/2021 14:43:44'!
for: anObserver

	^self new initializeFor: anObserver ! !
