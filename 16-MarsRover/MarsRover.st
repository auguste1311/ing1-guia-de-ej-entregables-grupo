!classDefinition: #TestMarsRover category: 'MarsRover'!
TestCase subclass: #TestMarsRover
	instanceVariableNames: 'north south east west'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!TestMarsRover methodsFor: 'setup' stamp: 'AR 10/6/2021 17:19:35'!
setUp

	north := North new.
	south := South new.
	east := East new.
	west := West new.! !


!TestMarsRover methodsFor: 'invalid command' stamp: 'AR 10/7/2021 15:03:14'!
test01MarsRoverDoNothingOnAnEmptyCommandSequence

	self marsRoverStartingAt: (1@2) heading: north afterProcessing: '' endsUpAtPosition: 1@2 andEndsUpHeading: north.! !

!TestMarsRover methodsFor: 'invalid command' stamp: 'AR 10/7/2021 15:17:03'!
test07MarsRoverStopsWhenInvalidCommand

	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'frAb' endsUpAtPosition: 1@3 andEndsUpHeading: east.
	! !


!TestMarsRover methodsFor: 'rotation' stamp: 'AR 10/7/2021 15:14:27'!
test04MarsRoverCanRotateToTheLeft
	
	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'l' endsUpAtPosition: 1@2 andEndsUpHeading: west.
	
	self marsRoverStartingAt: (1@2) heading: west afterProcessing: 'l' endsUpAtPosition: 1@2 andEndsUpHeading: south.
		
	self marsRoverStartingAt: (1@2) heading: south afterProcessing: 'l' endsUpAtPosition: 1@2 andEndsUpHeading: east.
			
	self marsRoverStartingAt: (1@2) heading: east afterProcessing: 'l' endsUpAtPosition: 1@2 andEndsUpHeading: north.! !

!TestMarsRover methodsFor: 'rotation' stamp: 'AR 10/7/2021 15:15:35'!
test05MarsRoverCanRotateToTheRight
	
	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'r' endsUpAtPosition: 1@2 andEndsUpHeading: east.
	
	self marsRoverStartingAt: (1@2) heading: east afterProcessing: 'r' endsUpAtPosition: 1@2 andEndsUpHeading: south.
		
	self marsRoverStartingAt: (1@2) heading: south afterProcessing: 'r' endsUpAtPosition: 1@2 andEndsUpHeading: west.
			
	self marsRoverStartingAt: (1@2) heading: west afterProcessing: 'r' endsUpAtPosition: 1@2 andEndsUpHeading: north.! !


!TestMarsRover methodsFor: 'movement' stamp: 'AR 10/7/2021 15:12:06'!
test02MarsRoverCanMovesForward

	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'f' endsUpAtPosition: 1@3 andEndsUpHeading: north.
	
	self marsRoverStartingAt: (1@2) heading: south afterProcessing: 'f' endsUpAtPosition: 1@1 andEndsUpHeading: south.
		
	self marsRoverStartingAt: (1@2) heading: east afterProcessing: 'f' endsUpAtPosition: 2@2 andEndsUpHeading: east.
	
	self marsRoverStartingAt: (1@2) heading: west afterProcessing: 'f' endsUpAtPosition: 0@2 andEndsUpHeading: west.! !

!TestMarsRover methodsFor: 'movement' stamp: 'AR 10/7/2021 15:13:14'!
test03MarsRoverCanMoveBackward

	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'b' endsUpAtPosition: 1@1 andEndsUpHeading: north.
	
	self marsRoverStartingAt: (1@2) heading: south afterProcessing: 'b' endsUpAtPosition: 1@3 andEndsUpHeading: south.
	
	self marsRoverStartingAt: (1@2) heading: east afterProcessing: 'b' endsUpAtPosition: 0@2 andEndsUpHeading: east.
	
	self marsRoverStartingAt: (1@2) heading: west afterProcessing: 'b' endsUpAtPosition: 2@2 andEndsUpHeading: west.
! !


!TestMarsRover methodsFor: 'multiple commands' stamp: 'AR 10/7/2021 15:16:45'!
test06MarsRoverCanHandleMultipleCommands

	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'lf' endsUpAtPosition: 0@2 andEndsUpHeading: west.
	
	self marsRoverStartingAt: (1@2) heading: north afterProcessing: 'frb' endsUpAtPosition: 0@3 andEndsUpHeading: east.! !


!TestMarsRover methodsFor: 'utils' stamp: 'AR 10/7/2021 15:02:15'!
marsRoverStartingAt: aPosition heading: aCardinalDirection afterProcessing: aCommandString 
	endsUpAtPosition: finalPosition andEndsUpHeading: finalCardinalDirection
	
	|marsRover|
	
	marsRover _ MarsRover at: aPosition heading: aCardinalDirection.
	
	marsRover process: aCommandString.
	
	self assert: ( marsRover  isAt: finalPosition heading: finalCardinalDirection)! !


!classDefinition: #CardinalDirection category: 'MarsRover'!
Object subclass: #CardinalDirection
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!CardinalDirection methodsFor: 'rotation' stamp: 'ts 10/7/2021 00:47:53'!
rotate90DegreesToTheLeft

	self subclassResponsibility .! !

!CardinalDirection methodsFor: 'rotation' stamp: 'ts 10/7/2021 00:47:57'!
rotate90DegreesToTheRight

	self subclassResponsibility .! !


!CardinalDirection methodsFor: 'representation' stamp: 'ts 10/7/2021 01:06:11'!
cartesianRepresentation

	self subclassResponsibility .! !


!classDefinition: #East category: 'MarsRover'!
CardinalDirection subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!East methodsFor: 'representation' stamp: 'ts 10/7/2021 01:06:11'!
cartesianRepresentation

	^ (1@0).! !


!East methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:58:40'!
rotate90DegreesToTheLeft

	^ North new.! !

!East methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:59:20'!
rotate90DegreesToTheRight
	
	^ South new.! !


!classDefinition: #North category: 'MarsRover'!
CardinalDirection subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!North methodsFor: 'representation' stamp: 'ts 10/7/2021 01:06:11'!
cartesianRepresentation

	^ (0@1).! !


!North methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:58:40'!
rotate90DegreesToTheLeft
	
	^ West new.! !

!North methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:59:20'!
rotate90DegreesToTheRight
	
	^ East new.! !


!classDefinition: #South category: 'MarsRover'!
CardinalDirection subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!South methodsFor: 'representation' stamp: 'ts 10/7/2021 01:06:11'!
cartesianRepresentation

	^ (0@-1).! !


!South methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:58:40'!
rotate90DegreesToTheLeft
	
	^ East new.! !

!South methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:59:20'!
rotate90DegreesToTheRight
	
	^ West new.! !


!classDefinition: #West category: 'MarsRover'!
CardinalDirection subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!West methodsFor: 'representation' stamp: 'ts 10/7/2021 01:06:11'!
cartesianRepresentation

	^(-1@0)! !


!West methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:58:40'!
rotate90DegreesToTheLeft
	
	^ South new.! !

!West methodsFor: 'rotation' stamp: 'ts 10/6/2021 23:59:20'!
rotate90DegreesToTheRight
	
	^ North new.! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'position heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'acting on commands' stamp: 'ts 10/7/2021 01:08:12'!
moveBackwards

	position _ position - heading cartesianRepresentation.	! !

!MarsRover methodsFor: 'acting on commands' stamp: 'ts 10/7/2021 01:08:12'!
moveForward

	position _ position + heading cartesianRepresentation.! !

!MarsRover methodsFor: 'acting on commands' stamp: 'ts 10/7/2021 01:08:12'!
rotateLeft

	heading _ heading rotate90DegreesToTheLeft.! !

!MarsRover methodsFor: 'acting on commands' stamp: 'ts 10/7/2021 01:08:12'!
rotateRight

	heading _ heading rotate90DegreesToTheRight.! !


!MarsRover methodsFor: 'position assertion' stamp: 'ts 10/7/2021 01:08:12'!
isAt: aPoint heading: aClass 
	
	^(position = aPoint and: heading className = aClass className)! !


!MarsRover methodsFor: 'initialization' stamp: 'ts 10/7/2021 01:08:12'!
initializeAt: anXYPoint heading: aCardinalDirection 

	position _ anXYPoint .
	heading _ aCardinalDirection .! !


!MarsRover methodsFor: 'processing commands' stamp: 'ts 10/7/2021 01:27:14'!
isValidCommand: aPotentialCommandCharacter

	^'fblr' includesSubString: (aPotentialCommandCharacter asString)! !

!MarsRover methodsFor: 'processing commands' stamp: 'AR 10/7/2021 14:32:01'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand |
			(self isValidCommand: aCommand) ifFalse: [^'Unrecognized command detected. Execution stopped'].
			(aCommand = $f)  ifTrue: [self moveForward ].
			(aCommand = $b)  ifTrue: [self moveBackwards ]. 
			(aCommand = $l)   ifTrue: [self rotateLeft ]. 
			(aCommand = $r)   ifTrue: [self rotateRight ].
		]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'class initialization' stamp: 'ts 10/7/2021 00:50:18'!
at: anXYPoint heading: aCardinalDirection

	^self new initializeAt: anXYPoint heading: aCardinalDirection! !
