!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'AR 9/8/2021 16:29:58'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds
	| customerBook paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	self assertTimeToExecute: [customerBook addCustomerNamed: paulMcCartney] isLessThan: 50.! !

!CustomerBookTest methodsFor: 'testing' stamp: 'AR 9/8/2021 16:29:58'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	
	self assertTimeToExecute: [customerBook removeCustomerNamed: paulMcCartney] isLessThan: 100.
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'au 9/9/2021 15:26:06'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	self assertFailureOnExecuting: [ customerBook addCustomerNamed: ''. self fail] withResult: Error compliesWith: [:anError | self assert: anError messageText = 		CustomerBook customerCanNotBeEmptyErrorMessage.			self assert: customerBook isEmpty] . ! !

!CustomerBookTest methodsFor: 'testing' stamp: 'au 9/9/2021 15:26:19'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self assertFailureOnExecuting: [ customerBook removeCustomerNamed: 'Paul McCartney'. self fail ] withResult: NotFound compliesWith: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon)]
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'au 9/9/2021 15:28:09'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	
	customerBook := self createCustomerBookWithSuspendedUser: paulMcCartney.
	
	self assertThatInCustomerBook: customerBook numberOfActiveCostumersIs: 0 numberOfSuspendedCostumersIs: 1 numberOfCostumersIs: 1.
	
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'au 9/9/2021 15:28:21'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	
	customerBook := self createCustomerBookWithSuspendedUser: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assertThatInCustomerBook: customerBook numberOfActiveCostumersIs: 0 numberOfSuspendedCostumersIs: 0 numberOfCostumersIs: 0.
	
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'au 9/9/2021 15:42:42'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self assertTheOnlyCostumerIn: customerBook IsJohnLennonAfterBeingUnableToSuspend: 'George Harrison' ! !

!CustomerBookTest methodsFor: 'testing' stamp: 'au 9/9/2021 15:42:48'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := self createCustomerBookWithSuspendedUser: johnLennon.
		
	self assertTheOnlyCostumerIn: customerBook IsJohnLennonAfterBeingUnableToSuspend: johnLennon 
	! !


!CustomerBookTest methodsFor: 'utils' stamp: 'au 9/9/2021 15:27:25'!
assertFailureOnExecuting: aClosure withResult: anError compliesWith: anAssertionClosure

	aClosure
		on: anError
		do: anAssertionClosure! !

!CustomerBookTest methodsFor: 'utils' stamp: 'au 9/9/2021 15:30:07'!
assertThatInCustomerBook: aCustomerBook numberOfActiveCostumersIs: aQuantityOfActiveCostumers numberOfSuspendedCostumersIs: aQuantityOfSuspendedCostumers numberOfCostumersIs: aQuantityOfCostumers

	self assert: aQuantityOfActiveCostumers equals: aCustomerBook numberOfActiveCustomers.
	self assert: aQuantityOfSuspendedCostumers equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: aQuantityOfCostumers equals: aCustomerBook numberOfCustomers.	! !

!CustomerBookTest methodsFor: 'utils' stamp: 'au 9/9/2021 15:42:24'!
assertTheOnlyCostumerIn: aCustomerBook IsJohnLennonAfterBeingUnableToSuspend: aCustomer

	[ aCustomerBook suspendCustomerNamed: aCustomer.
		self fail ]
			on: CantSuspend 
			do: [ :anError | 
				self assert: aCustomerBook numberOfCustomers = 1.
				self assert: (aCustomerBook includesCustomerNamed: 'John Lennon') ]
	! !

!CustomerBookTest methodsFor: 'utils' stamp: 'AR 9/8/2021 16:29:58'!
assertTimeToExecute: aClosure isLessThan: millisecondAmount
	
	|millisecondsBeforeRunning millisecondsAfterRunning|
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClosure.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < (millisecondAmount * millisecond)! !

!CustomerBookTest methodsFor: 'utils' stamp: 'AR 9/8/2021 17:19:53'!
createCustomerBookWithSuspendedUser: userName
	|customerBook|
	
	customerBook := CustomerBook new.	
	customerBook addCustomerNamed: userName.
	customerBook suspendCustomerNamed: userName.
	
	^customerBook.! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'au 9/9/2021 15:39:18'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName ) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'AR 9/8/2021 16:28:29'!
numberOfCustomers
	
	^self numberOfActiveCustomers + self numberOfSuspendedCustomers! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'AR 9/8/2021 16:27:33'!
removeCustomerNamed: aName 
 
	^ active remove: aName ifAbsent: [suspended remove: aName ifAbsent: [^NotFound signal]]
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:13'!
customerAlreadyExistsErrorMessage

	^'customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:09'!
customerCanNotBeEmptyErrorMessage

	^'customer name cannot be empty!!!!!!'! !
